/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include "SoUtils/EllipticalPrism.h"
// ============================================================================
// HepVis
// ============================================================================
#include <Inventor/nodes/SoSphere.h>
// SoSphere must be included before SoEllipticalPrism
#include <HEPVis/nodes/SoEllipticalPrism.h>
// ============================================================================
// local
// ============================================================================
#include "SoUtils/EigenSystems.h"
// ============================================================================

// ============================================================================
/** @file
 *
 *  Implementation file for SoUtils::ellipticalprism
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 22/11/2001
 */
// ============================================================================

// ============================================================================
/** create SoEllipticalPrism Inventor node from the
 *  covarinace matrix and position
 *  values of error codes from "eigensystem" method
 *  @param center position of elliptical prism
 *  @param cov    "covarinace" matrix (2D) for the ellipsoid
 *  @param extent extent for elliptical prism
 *  @param node   (return) pointer to created SoEllipsoid Inventor node
 *  @return status code
 */
// ============================================================================
StatusCode SoUtils::ellipticalprism( const Gaudi::XYZPoint& center, const Gaudi::SymMatrix2x2& cov, const double extent,
                                     SoEllipticalPrism*& node ) {
  /// reset the output
  node = 0;

  if ( cov == 0 ) {
    /// return StatusCode::FAILURE;
  }
  Gaudi::Vector3              evals( 0, 0, 0 );
  std::vector<Gaudi::Vector3> evects;
  // OD StatusCode sc = SoUtils::eigensystem( cov , evals , evects );
  // if( sc.isFailure       () ) { return sc                  ; } ///< RETURN !
  if ( 2 != evects.size() ) { return StatusCode::FAILURE; } ///< RETURN !
  //  if( 2 != evals.num_row () ) { return StatusCode::FAILURE ; } ///< RETURN !
  ///
  SoEllipticalPrism* ell = new SoEllipticalPrism();
  ell->center.setValue( (float)center.x(), (float)center.y(), (float)center.z() );
  ell->eigenvalues.setValue( (float)sqrt( evals( 1 ) ), (float)sqrt( evals( 2 ) ) );
  ///

  Gaudi::Rotation3D rot;
  Gaudi::XYZVector  vx( evects[0]( 1 ), evects[0]( 2 ), 0 );
  Gaudi::XYZVector  vy( evects[1]( 1 ), evects[1]( 2 ), 0 );
  ///
  // OD  rot.rotateAxes ( vx , vy , vx.cross( vy ) );
  Gaudi::XYZVector axis;
  double           angle = 0;
  // OD rot.getAngleAxis( angle , axis );
  ell->rotation.setValue( SbVec3f( (float)axis.x(), (float)axis.y(), (float)axis.z() ), (float)angle );
  ell->extent.setValue( (float)extent );
  ///
  node = ell;
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
// The End
// ============================================================================
