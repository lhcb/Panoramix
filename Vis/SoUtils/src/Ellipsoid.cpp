/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// HepVis
// ============================================================================
#include <HEPVis/nodes/SoEllipsoid.h>
#include <Inventor/nodes/SoSphere.h>
// ============================================================================
// local
// ============================================================================
#include "SoUtils/EigenSystems.h"
#include "SoUtils/Ellipsoid.h"
// ============================================================================

// ============================================================================
/** @file
 *
 *  Implementation file for SoUtils::ellipsoid
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 22/11/2001
 */
// ============================================================================

// ============================================================================
/** create SoEllipsoid Inventor node from the
 *  covarinace matrix and position
 *  values of error codes from "eigensystem" method
 *  @param center positinoi of ellipsoid center
 *  @param cov    "covarinace" matrix for the ellipsoid
 *  @param node   (return) pointer to created SoEllipsoid Inventor node
 *  @return status code
 */
// ============================================================================
StatusCode SoUtils::ellipsoid( const Gaudi::XYZPoint& center, const Gaudi::SymMatrix3x3& cov, SoEllipsoid*& node ) {
  // reset the output
  node = 0;
  //  if( 3 != cov.num_row()    ) { return StatusCode::FAILURE ; } ///< RETURN !
  //
  Gaudi::Vector3                      evals( 0, 0, 0 );
  typedef std::vector<Gaudi::Vector3> Vectors;
  Vectors                             evects;
  StatusCode                          sc = SoUtils::eigensystem( cov, evals, evects );
  if ( sc.isFailure() ) { return sc; }                      ///< RETURN !
  if ( 3 != evects.size() ) { return StatusCode::FAILURE; } ///< RETURN !
  //  if( 3 != evals.num_row () ) { return StatusCode::FAILURE ; } ///< RETURN !
  //

  SoEllipsoid* ell = new SoEllipsoid();
  ell->center.setValue( (float)center.x(), (float)center.y(), (float)center.z() );
  ell->eigenvalues.setValue( (float)sqrt( evals( 0 ) ), (float)sqrt( evals( 1 ) ), (float)sqrt( evals( 2 ) ) );
  //
  const Gaudi::XYZVector vx( evects[0]( 0 ), evects[0]( 1 ), evects[0]( 2 ) );
  const Gaudi::XYZVector vy( evects[1]( 0 ), evects[1]( 1 ), evects[1]( 2 ) );
  //
  const Gaudi::XYZVector   vz = vx.Cross( vy );
  const Gaudi::Transform3D rot(
      Gaudi::Rotation3D( vx.X(), vy.X(), vz.X(), vx.Y(), vy.Y(), vz.Y(), vx.Z(), vy.Z(), vz.Z() ) );
  //
  ROOT::Math::AxisAngle axisangle;
  rot.GetRotation( axisangle );
  ell->rotation.setValue(
      SbVec3f( (float)axisangle.Axis().x(), (float)axisangle.Axis().y(), (float)axisangle.Axis().z() ),
      (float)axisangle.Angle() );
  //
  node = ell;
  //
  return StatusCode::SUCCESS;
}

// ============================================================================
// The End
// ============================================================================
