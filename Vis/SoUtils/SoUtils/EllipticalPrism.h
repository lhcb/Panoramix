/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: EllipticalPrism.h,v 1.4 2008-07-28 08:11:22 truf Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.3  2007/03/19 15:23:33  ranjard
// v3r2 - fix for LHCb v22r2
//
// Revision 1.2  2006/03/09 16:48:15  odescham
// v2r1 - migrated to LHCb v20r0 - to be completed
//
// Revision 1.1.1.1  2004/09/08 15:52:31  ibelyaev
// New package: code moved from Vis/SoCalo
//
// ============================================================================
#ifndef SOUTILS_ELLIPTICALPRISM_H
#  define SOUTILS_ELLIPTICALPRISM_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#  include <vector>
// From GaudiKernel
#  include "GaudiKernel/Point3DTypes.h"
#  include "GaudiKernel/SymmetricMatrixTypes.h"
#  include "GaudiKernel/Transform3DTypes.h"
#  include "GaudiKernel/Vector3DTypes.h"
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/StatusCode.h"
// ============================================================================
/// forward declarations
// ============================================================================
class SoEllipsoid;       ///< from HEPVis package
class SoEllipticalPrism; ///< from HepVis package
// ============================================================================

namespace SoUtils {
  /** @fn ellipticalprism
   *  create SoEllipticalPrism Inventor node from the
   *  covarinace matrix and position
   *  values of error codes from "eigensystem" method
   *  @param center  position of elliptical prism
   *  @param cov     "covarinace" matrix (2D) for the ellipsoid
   *  @param extent  extent for elliptical prism
   *  @param node    (return) pointer to created SoEllipsoid Inventor node
   *  @return status code
   */
  StatusCode ellipticalprism( const Gaudi::XYZPoint& center, const Gaudi::SymMatrix2x2& cov, const double extend,
                              SoEllipticalPrism*& node );
} // namespace SoUtils

// ============================================================================
// The END
// ============================================================================
#endif // SOUTILS_ELLIPTICALPRISM_H
// ============================================================================
