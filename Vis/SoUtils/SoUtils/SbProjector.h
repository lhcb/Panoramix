/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoUtils_SbProjector_h
#define SoUtils_SbProjector_h

#include <Inventor/SbLinear.h>
#include <Inventor/SbString.h>

/*!
 * class SbProjector
 *
 * To handle special (non-linear) projections (rz, phiz, etc...)
 */
namespace SoUtils {

  class SbProjector {
  public:
    SbProjector( const SbString& );
    SbProjector( const SbProjector& );
    virtual ~SbProjector();
    bool project( int, SbVec3f* ) const;
    bool isRZ() const;
    bool isZR() const;

  protected:
    bool projectRZ( int, SbVec3f* ) const;
    bool projectPHIZ( int, SbVec3f* ) const;
    bool projectZR( int, SbVec3f* ) const;
    bool projectNZR( int, SbVec3f* ) const;
    bool projectYZR( int, SbVec3f* ) const;
    bool projectNormZ( int, SbVec3f* ) const;
    bool projectZPHI( int, SbVec3f* ) const;
    bool projectPHIETA( int, SbVec3f* ) const;

  private:
    int fProjection;
  };

} // namespace SoUtils

#endif
