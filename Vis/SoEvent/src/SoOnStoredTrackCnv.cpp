/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SoOnStoredTrack.h"

#include "Event/OTClusterOnStoredTrack.h"
// static CnvFactory< SoOTOnStoredTrackCnv<OTClusterOnStoredTrack,OTTime> > s_OT_factory;
// extern const ICnvFactory& SoOTClusterOnStoredTrackCnvFactory = s_OT_factory;
DECLARE_CONVERTER( SoOTClusterOnStoredTrackCnv );

#include "Event/ITClusterOnStoredTrack.h"
// static CnvFactory< SoOnStoredTrackCnv<ITClusterOnStoredTrack,ITCluster> > s_IT_factory;
// extern const ICnvFactory& SoITClusterOnStoredTrackCnvFactory = s_IT_factory;
DECLARE_CONVERTER( SoITClusterOnStoredTrackCnv );

#include "Event/VeloClusterOnStoredTrack.h"
// static CnvFactory< SoOnStoredTrackCnv<VeloClusterOnStoredTrack,VeloCluster> > s_Velo_factory;
// extern const ICnvFactory& SoVeloClusterOnStoredTrackCnvFactory = s_Velo_factory;
DECLARE_CONVERTER( SoVeloClusterOnStoredTrackCnv );
