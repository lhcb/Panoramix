/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEventConverter_h
#define SoEventConverter_h

// Inheritance :
#include "GaudiKernel/Converter.h"
#include "Kernel/IParticlePropertySvc.h"

class ISoConversionSvc;
class IUserInterfaceSvc;
class IParticlePropertySvc;
class IDataProviderSvc;

class SoEventConverter : public Converter {
public:
  SoEventConverter( ISvcLocator*, CLID );
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual long       repSvcType() const;

protected:
  ISoConversionSvc*           fSoCnvSvc;
  IUserInterfaceSvc*          fUISvc;
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
  IDataProviderSvc*           fDetectorDataSvc;
};

#endif
