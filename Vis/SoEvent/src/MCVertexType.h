/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_MCVertexType_h
#define SoEvent_MCVertexType_h

// Inheritance :
#include "Type.h"

#include "Event/MCVertex.h"
#include "Kernel/IParticlePropertySvc.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;

class MCVertexType : public SoEvent::Type<LHCb::MCVertex> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  MCVertexType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc*, LHCb::IParticlePropertySvc* );

private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
