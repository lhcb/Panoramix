/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "MCParticleType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Out.h"
#include "Lib/Variable.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

// Event model :
#include "Event/CaloDigit.h"
#include "Event/FTCluster.h"
#include "Event/MCHit.h"
#include "Event/MuonCoord.h"
#include "Event/MuonDigit.h"
#include "Event/Track.h"
#include "Event/UTCluster.h"
#include "Event/VPCluster.h"

// MC relations:
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedFromKey.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"

//////////////////////////////////////////////////////////////////////////////
MCParticleType::MCParticleType( IUserInterfaceSvc* aUISvc, ISoConversionSvc* aSoCnvSvc,
                                IDataProviderSvc* aDataProviderSvc, LHCb::IParticlePropertySvc* aParticlePropertySvc )
    : SoEvent::Type<LHCb::MCParticle>( LHCb::MCParticles::classID(), "MCParticle",
                                       "/Event/" + LHCb::MCParticleLocation::Default, aUISvc, aSoCnvSvc,
                                       aDataProviderSvc )
    , fParticlePropertySvc( aParticlePropertySvc )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  addProperty( "key", Lib::Property::INTEGER );
  addProperty( "particle", Lib::Property::STRING );
  addProperty( "pid", Lib::Property::INTEGER );
  addProperty( "energy", Lib::Property::DOUBLE, 0, "(MeV)" );
  addProperty( "pt", Lib::Property::DOUBLE, 0, "(MeV)" );
  addProperty( "charge", Lib::Property::DOUBLE, 6 );
  addProperty( "mass", Lib::Property::DOUBLE, 0, "(MeV)" );
  addProperty( "decayLength", Lib::Property::DOUBLE, 0, "(mm)" );
  addProperty( "decayVertices", Lib::Property::INTEGER );
  addProperty( "parent", Lib::Property::STRING );
  addProperty( "parentID", Lib::Property::INTEGER );
  addProperty( "parent2", Lib::Property::STRING );
  addProperty( "timeOfFlight", Lib::Property::DOUBLE, 0, "(ns)" );
  addProperty( "bcflag", Lib::Property::INTEGER );
  addProperty( "address", Lib::Property::POINTER );
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MCParticleType::value( Lib::Identifier aIdentifier, const std::string& aName, void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fUISvc->session()->setParameter( "modeling.what", "no" );
  LHCb::MCParticle* obj = (LHCb::MCParticle*)aIdentifier;

  if ( aName == "address" ) {
    return Lib::Variable( printer(), (void*)obj );
  } else if ( aName == "key" ) {
    return Lib::Variable( printer(), obj->key() );
  } else if ( aName == "energy" ) {
    return Lib::Variable( printer(), obj->momentum().e() );
  } else if ( aName == "pt" ) {
    double px = obj->momentum().px();
    double py = obj->momentum().py();
    return Lib::Variable( printer(), sqrt( px * px + py * py ) );
  } else if ( aName == "mass" ) {
    double px = obj->momentum().px();
    double py = obj->momentum().py();
    double pz = obj->momentum().pz();
    double e  = obj->momentum().e();
    return Lib::Variable( printer(), sqrt( e * e - ( px * px + py * py + pz * pz ) ) );
  } else if ( aName == "particle" ) {
    if ( fParticlePropertySvc ) {
      const LHCb::ParticleProperty* pp = fParticlePropertySvc->find( obj->particleID() );
      if ( pp ) return Lib::Variable( printer(), pp->particle() );
    }
    return Lib::Variable( printer(), std::string( "nil" ) );
  } else if ( aName == "charge" ) {
    if ( fParticlePropertySvc ) {
      const LHCb::ParticleProperty* pp = fParticlePropertySvc->find( obj->particleID() );
      if ( pp ) return Lib::Variable( printer(), pp->charge() );
    }
    return Lib::Variable( printer(), 0. );
  } else if ( aName == "pid" ) {
    return Lib::Variable( printer(), (int)( obj->particleID().pid() ) );
  } else if ( aName == "decayVertices" ) {
    return Lib::Variable( printer(), (int)( obj->endVertices().size() ) );
  } else if ( aName == "parent" ) {
    const LHCb::MCParticle* mcParticle = ( obj->mother() );
    if ( mcParticle ) {
      if ( fParticlePropertySvc ) {
        const LHCb::ParticleProperty* pp = fParticlePropertySvc->find( mcParticle->particleID() );
        if ( pp ) return Lib::Variable( printer(), pp->particle() );
      }
    }
    return Lib::Variable( printer(), std::string( "nil" ) );
  } else if ( aName == "parentID" ) {
    const LHCb::MCParticle* mcParticle = ( obj->mother() );
    if ( mcParticle ) { return Lib::Variable( printer(), (int)mcParticle->particleID().pid() ); }
    return Lib::Variable( printer(), (int)-2 );
  } else if ( aName == "parent2" ) {
    const LHCb::MCParticle* mcParticle1 = ( obj->mother() );
    if ( mcParticle1 ) {
      const LHCb::MCParticle* mcParticle = ( mcParticle1->mother() );
      if ( mcParticle ) {
        if ( fParticlePropertySvc ) {
          const LHCb::ParticleProperty* pp = fParticlePropertySvc->find( mcParticle->particleID() );
          if ( pp ) return Lib::Variable( printer(), pp->particle() );
        }
      }
    }
    return Lib::Variable( printer(), std::string( "nil" ) );
  } else if ( aName == "decayLength" ) {
    const LHCb::MCVertex* mcVertex = obj->originVertex();
    if ( !mcVertex ) return Lib::Variable( printer(), (double)-1 );
    const SmartRefVector<LHCb::MCVertex>& decayVertices = obj->endVertices();
    int                                   number        = decayVertices.size();
    if ( number <= 0 ) return Lib::Variable( printer(), (double)-2 );
    const Gaudi::XYZPoint pos  = mcVertex->position();
    const Gaudi::XYZPoint pos2 = ( *( decayVertices[0] ) ).position();
    double                dx   = pos2.x() - pos.x();
    double                dy   = pos2.y() - pos.y();

    double dz = pos2.z() - pos.z();
    return Lib::Variable( printer(), sqrt( dx * dx + dy * dy + dz * dz ) );
  } else if ( aName == "timeOfFlight" ) {
    const LHCb::MCVertex* mcVertex = obj->originVertex();
    if ( !mcVertex ) return Lib::Variable( printer(), (double)-1 );
    const SmartRefVector<LHCb::MCVertex>& decayVertices = obj->endVertices();
    int                                   number        = decayVertices.size();
    if ( number <= 0 ) return Lib::Variable( printer(), (double)-2 );

    return Lib::Variable( printer(), (double)( *( decayVertices[0] ) ).time() );
  } else if ( aName == "bcflag" ) {
    int                                bcflag = 0;
    const LHCb::MCVertex::MCVertexType a      = ( obj->originVertex() )->type();
    if ( ( a == LHCb::MCVertex::DecayVertex ) || ( a == LHCb::MCVertex::OscillatedAndDecay ) ||
         ( a == LHCb::MCVertex::ppCollision ) || ( a == LHCb::MCVertex::Unknown ) ) {
      const LHCb::MCParticle* mcParticle = obj;
      while ( mcParticle ) {
        if ( mcParticle->particleID().hasBottom() ) { bcflag += 10; }
        if ( mcParticle->particleID().hasCharm() ) { bcflag += 1; }
        mcParticle = ( mcParticle->mother() );
      }
    }
    return Lib::Variable( printer(), bcflag );
  } else {
    return Lib::Variable( printer() );
  }
}
//////////////////////////////////////////////////////////////////////////////
void MCParticleType::visualize( Lib::Identifier aIdentifier, void* aData )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if ( !aIdentifier ) return;
  if ( !fUISvc ) return;
  if ( !fUISvc->session() ) return;
  std::string value;
  fUISvc->session()->parameterValue( "modeling.what", value );

  if ( value == "Track" ) {

    LHCb::MCParticle* object = (LHCb::MCParticle*)aIdentifier;
    visualizeTrack( *object );

  } else if ( value == "Clusters" ) {

    LHCb::MCParticle* object = (LHCb::MCParticle*)aIdentifier;
    visualizeClusters( *object );

  } else if ( value == "MCHits" ) {

    LHCb::MCParticle* object = (LHCb::MCParticle*)aIdentifier;
    visualizeHits( *object );

  } else {

    this->SoEvent::Type<LHCb::MCParticle>::visualize( aIdentifier, aData );
  }
}
//////////////////////////////////////////////////////////////////////////////
void MCParticleType::visualizeHits( LHCb::MCParticle& aParticle )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // plot associated MCHits
  if ( !fDataProviderSvc ) return;

  std::string location;

  for ( int k = 0; k < 6; k++ ) {

    if ( k == 0 ) { location = LHCb::MCHitLocation::IT; }
    if ( k == 1 ) { location = LHCb::MCHitLocation::TT; }
    if ( k == 2 ) { location = LHCb::MCHitLocation::OT; }
    if ( k == 3 ) { location = LHCb::MCHitLocation::Velo; }
    if ( k == 4 ) { location = LHCb::MCHitLocation::Muon; }
    DataObject* dataObject;
    // StatusCode sc =
    fDataProviderSvc->retrieveObject( location, dataObject ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    LHCb::MCHits* hits = dynamic_cast<LHCb::MCHits*>( dataObject );

    for ( LHCb::MCHits::iterator it = hits->begin(); it != hits->end(); it++ ) {

      LHCb::MCParticle* obj = const_cast<LHCb::MCParticle*>( ( *it )->mcParticle() );
      if ( aParticle.key() == obj->key() ) {
        LHCb::MCHits* objs = new LHCb::MCHits;
        LHCb::MCHit*  aHit = *it;
        objs->add( aHit );
        // Convert it :
        IOpaqueAddress* addr = 0;
        StatusCode      sc   = fSoCnvSvc->createRep( objs, addr );
        if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs );
        objs->remove( obj );
        delete objs;
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
void MCParticleType::visualizeTrack( LHCb::MCParticle& aParticle )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // get current track location
  std::string location;
  fUISvc->session()->parameterValue( "Track.location", location );
  if ( location == "" ) { location = LHCb::TrackLocation::Default; }

  LinkedFrom<LHCb::Track, LHCb::MCParticle> track3dLink( fDataProviderSvc, 0, location );

  if ( track3dLink.notFound() ) {
  } else {
    LHCb::Track* obj = track3dLink.first( &aParticle );
    while ( obj ) {
      auto objs = std::make_unique<LHCb::Tracks>();
      objs->add( obj );
      // Convert it :
      IOpaqueAddress* addr = nullptr;
      StatusCode      sc   = fSoCnvSvc->createRep( objs.get(), addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs.get() );
      objs->remove( obj );
      obj = track3dLink.next();
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
void MCParticleType::visualizeClusters( LHCb::MCParticle& aParticle )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LinkedFrom<LHCb::VPCluster, LHCb::MCParticle> VPClusterLink( fDataProviderSvc, 0, LHCb::VPClusterLocation::Default );

  if ( VPClusterLink.notFound() ) {
  } else {
    LHCb::VPCluster* obj = VPClusterLink.first( &aParticle );
    while ( 0 != obj ) {
      auto objs = std::make_unique<LHCb::VPClusters>();
      objs->add( obj );
      // Convert it :
      IOpaqueAddress* addr = nullptr;
      StatusCode      sc   = fSoCnvSvc->createRep( objs.get(), addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs.get() );
      objs->remove( obj );
      obj = VPClusterLink.next();
    }
  }
  /* is some funcvtionality needed here? PK-R3C
  LinkedFrom<LHCb::FTCluster, LHCb::MCParticle> FTClusterLink( fDataProviderSvc, 0,
                                                               LHCb::FTClusterLocation::FTClusters );

  if ( FTClusterLink.notFound() ) {
  } else {
    LHCb::FTCluster* obj = FTClusterLink.first( &aParticle );
    while ( 0 != obj ) {
      LHCb::FTClusters* objs = new LHCb::FTClusters;
      objs->add( obj );
      // Convert it :
      IOpaqueAddress* addr = 0;
      StatusCode      sc   = fSoCnvSvc->createRep( objs, addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs );
      objs->remove( obj );
      delete objs;
      obj = FTClusterLink.next();
    }
  }
  */
  LinkedFrom<LHCb::UTCluster, LHCb::MCParticle> UTClusterLink( fDataProviderSvc, 0,
                                                               LHCb::UTClusterLocation::UTClusters );
  if ( UTClusterLink.notFound() ) {
  } else {
    LHCb::UTCluster* obj = UTClusterLink.first( &aParticle );
    while ( obj ) {
      auto objs = std::make_unique<LHCb::UTClusters>();
      objs->add( obj );
      // Convert it :
      IOpaqueAddress* addr = nullptr;
      StatusCode      sc   = fSoCnvSvc->createRep( objs.get(), addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs.get() );
      objs->remove( obj );
      obj = UTClusterLink.next();
    }
  }

  LinkedFrom<LHCb::MuonCoord, LHCb::MCParticle> MuonCoordLink( fDataProviderSvc, 0,
                                                               LHCb::MuonCoordLocation::MuonCoords );
  if ( MuonCoordLink.notFound() ) {
  } else {
    LHCb::MuonCoord* obj = MuonCoordLink.first( &aParticle );
    while ( obj ) {
      auto objs = std::make_unique<LHCb::MuonCoords>();
      objs->add( obj );
      // Convert it :
      IOpaqueAddress* addr = nullptr;
      StatusCode      sc   = fSoCnvSvc->createRep( objs.get(), addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs.get() );
      objs->remove( obj );
      obj = MuonCoordLink.next();
    }
  }
  // does not really work, I think. TR July 2011
  LinkedFrom<LHCb::MuonDigit, LHCb::MCParticle> MuonDigitLink( fDataProviderSvc, 0,
                                                               LHCb::MuonDigitLocation::MuonDigit );
  if ( MuonDigitLink.notFound() ) {
  } else {
    DataObject* dataObject( nullptr );
    fDataProviderSvc->retrieveObject( LHCb::MuonCoordLocation::MuonCoords, dataObject ).ignore();
    LHCb::MuonCoords* rawCoords = dynamic_cast<LHCb::MuonCoords*>( dataObject );
    LHCb::MuonDigit*  obj       = MuonDigitLink.first( &aParticle );
    while ( obj ) {
      LHCb::MuonCoord* mobj =
          dynamic_cast<LHCb::MuonCoord*>( rawCoords->object( LHCb::Detector::Muon::TileID( obj->index() ) ) );
      auto objs = std::make_unique<LHCb::MuonCoords>();
      objs->add( mobj );
      // Convert it :
      IOpaqueAddress* addr = nullptr;
      StatusCode      sc   = fSoCnvSvc->createRep( objs.get(), addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs.get() );
      objs->remove( mobj );
      obj = MuonDigitLink.next();
    }
  }
  LinkedFrom<LHCb::CaloDigit, LHCb::MCParticle> EcalLink( fDataProviderSvc, 0, LHCb::CaloDigitLocation::Ecal );
  if ( EcalLink.notFound() ) {
  } else {
    LHCb::CaloDigit* obj = EcalLink.first( &aParticle );
    while ( obj ) {
      auto objs = std::make_unique<LHCb::CaloDigits>();
      objs->add( obj );
      // Convert it :
      IOpaqueAddress* addr = nullptr;
      StatusCode      sc   = fSoCnvSvc->createRep( objs.get(), addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs.get() );
      objs->remove( obj );
      obj = EcalLink.next();
    }
  }
}
