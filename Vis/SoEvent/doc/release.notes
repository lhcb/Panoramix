!-----------------------------------------------------------------------------
! Package     : Vis/SoEvent
! Responsible : Thomas Ruf
! Purpose     :
!-----------------------------------------------------------------------------

!=========================== SoEvent v7r50 =====================================
! 2016-05-17 - Thomas Ruf
  try adding support for displaying lite clusters
! 2016-01-19 - Thomas Ruf

! 2016-01-06 - Marco Clemencic
 - Fixed compilation after modernization of Gaudi (auto_ptr -> unique_ptr).

! 2014-06-16 - Heinrich Schindler
 - Follow changes in VPMeasurement constructor (r173751).

! 2014-04-26 - Heinrich Schindler
 - Follow changes in VPChannelID format (r171869).

! 2014-04-01 - Heinrich Schindler
 - Replace VPLiteCluster, VPLiteMeasurement by VPCluster, VPMeasurement.
 - Remove VL leftovers in SoEventDict.xml.
 - Replace FTRawCluster by FTLiteCluster in SoFTClusterCnv.

!=========================== SoEvent v7r48 =====================================
! 2014-01-28 - Marco Clemencic
 - Added CMake configuration file.

! 2014-01-28 - Marco Clemencic
 - removed VL-related code (will not be used in the Upgrade)

! 2014-01-28 - Marco Clemencic
 - removed obsolete file

!=========================== SoEvent v7r47 =====================================
! 2013-05-14 - Thomas Ruf
  add support for upgrade detectors
!=========================== SoEvent v7r45 =====================================
! 2012-07-06 - Thomas Ruf
  removed include of Kernel/ReadRoutingBits.h in dict
  fixed problem in SoParticle when forcing to come from Vertex
!=========================== SoEvent v7r44 =====================================
! 2012-04-04 - Thomas Ruf
!=========================== SoEvent v7r43 =====================================
! 2012-02-06 - Thomas Ruf
  fixed problem with VertexType
  added possibility to draw particle from reference point to decay vertex
  ignoring flight direction
!=========================== SoEvent v7r42 =====================================
! 2012-01-27 - Thomas Ruf
  remove any use of old Gaudi ParticlePropertySvc
!=========================== SoEvent v7r41 =====================================
! 2011-11-22 - Thomas Ruf
!=========================== SoEvent v7r40 =====================================
! 2011-09-23 - Thomas Ruf
  take HltUtils from SoUtils
!=========================== SoEvent v7r39 =====================================
! 2011-09-20 - Thomas Ruf
  add L0Muon tools to dict
!=========================== SoEvent v7r38 =====================================
! 2011-07-18 - Thomas Ruf
  remove printing pointer address when plotting vertices
!=========================== SoEvent v7r37 =====================================
! 2011-05-13 - Thomas Ruf
 explicit use of DaVinciTypes
!=========================== SoEvent v7r36 =====================================
! 2011-02-21 - Thomas Ruf
 more missing dicts
!=========================== SoEvent v7r35 =====================================
! 2010-11-15 - Thomas Ruf
 added some missing dicts for HltUtils, Routingbits
!=========================== SoEvent v7r34 =====================================
! 2010-11-15 - Thomas Ruf
 SoParticleCnv, start end of particle trajectory
!=========================== SoEvent v7r33 =====================================
! 2010-10-11 - Thomas Ruf
 changes in SoSTCluster to follow changes in ST software
! 2010-09-23 - Thomas Ruf
 add boolean flag pvass to track type, true if associated to any PV, false else.
!=========================== SoEvent v7r32 =====================================
! 2010-09-13 - Thomas Ruf
!=========================== SoEvent v7r31 =====================================
! 2010-08-04 - Thomas Ruf
 fixed memory leak in SoTrackCnv
! 2010-07-15 - Jeroen VAN TILBURG
 - Changed include location of ISTClusterPosition (now in Tr/TrackInterfaces).
!=========================== SoEvent v7r30 =====================================
! 2010-07-14 - Thomas Ruf
 TrackType added more types
!=========================== SoEvent v7r29 =====================================
! 2010-06-02 - Thomas Ruf
 SoTrackCnv added option to display tracks without states
!=========================== SoEvent v7r28 =====================================
 SoTrackCnv compilation problem, and -> && fixed
!=========================== SoEvent v7r27 =====================================
! 2010-02-18 - Thomas Ruf
  - SoTrackCnv: improve display of backward tracks
!=========================== SoEvent v7r26 =====================================
! 2009-11-20 - Thomas Ruf
 - adapt to latest track class changes
!=========================== SoEvent v7r25 =====================================
! 2009-11-02 - Thomas Ruf
 - remove maps from dict building
! 2009-10-14 - Thomas Ruf
 - fix problem with MCParticle to OTTime link in MCParticleType.cpp

! 2009-10-10 - Wouter Hulsbergen
 - adapt to changes in LHCb::Track
!=========================== SoEvent v7r24 =====================================

! 2009-09-24 - Chris Jones
 - Call Service::finalize() or Converter::finalize() during finalize();

!=========================== SoEvent v7r23 =====================================
! 2009-08-25 - Thomas Ruf
 - requirements, matched to new CMT version

!=========================== SoEvent v7r22 =====================================
! 2009-07-27 - Thomas Ruf
 - SoTrackCnv, introduce -ZRY projection
! 2009-09-07 - Thomas Ruf
 - remove TrackTraj from dict building

!=========================== SoEvent v7r21 =====================================

! 2009-26-06 - Thomas Ruf
 - SoTrackCnv/SoVeloClusterCnv : some more fine tuning for RZ projection
   suppress crossing tracks

!=========================== SoEvent v7r20 =====================================

! 2009-07-04 - Guy Barrand
 - SoL0MuonCoordCnv : fix missing separator->addChild(sep)

! 2009-06-04 - Thomas Ruf
 - Fix missing separator->addChild(sep) in SoVeloClusterCnv.cpp

! 2009-06-03 - Chris Jones
 - Fix compilation warnings with gcc 4.3
 - Remove obsolete SoEvent_load.cpp file

! 2009-06-02 - T.Ruf
  - fixed OTTimeType for new linking to MC truth

! 2009-05-27 - T.Ruf
  - fixed an issue with extra qualifier in ParticleType.h only showing up
    with slc5 platform

! 2009-05-25 - Guy Barrand
 - * : use a SoSceneGraph to handle, with an SbString, the "pick id string".
   This permits to avoid a bottleneck if using an SbName
   (seen with Mac/Shark tool). (SbNames are handled in a global hash list,
   then intensive operations on them is time consuming).

!=========================== SoEvent v7r19 =====================================

!=========================== SoEvent v7r18 =====================================

! 2009-03-01 - C Jones
 - Remove a broken APPLE ifdef

!=========================== SoEvent v7r17 =====================================

! 2009-01-19 : T.Ruf
 - added CaloElectron tool to SoEventDict for tests
!=========================== SoEvent v7r16 =====================================

! 2008-12-02 : T.Ruf
  added dictionary for DeVeloSensor::StripInfo

!=========================== SoEvent v7r15 =====================================

! 2008-09-24 : T.Ruf
 - TrackType.cpp : display clusters in tae slot depending on track location
 - L0MuonCoord, support for displaying L0 muon objects, author:F.Rodrigues
! 2008-09-24 : G.Barrand
 - SoMCHitCnv.cpp : use the HEPVis/SoMarkerSet as for other cnv.


!=========================== SoEvent v7r14 =====================================
 - 2008-08-08 T.Ruf
 - added some missing dicts temporarly to SoEventDict
!=========================== SoEvent v7r13 =====================================
 - added some missing dicts temporarly to SoEventDict

!=========================== SoEvent v7r12 =====================================

! 2008-04-12 - Thomas Ruf
 - Added CaloDigit to K0 dictionary for Object_visualize
! 2008-03-18 - Guy Barrand
 - Add emissiveColor to material of lines.
   This is in order to save VRML properly.

!=========================== SoEvent v7r11 =====================================
! 2008-03-09 - Thomas Ruf
 - adapted to Gaudi v19r8, dict and src/TrackType.cpp
!=========================== SoEvent v7r10 =====================================
! 2008-02-22 - Guy Barrand
 - removed a last #ifdef OSC_VERSION_16_0 in Type.h

! 2008-02-21 - Thomas Ruf
 - added dicts for L0DUREPORT maps
! 2008-02-04 - Thomas Ruf
 - removed all #ifdef OSC_VERSION_16_0 and kept the code under #ifdef
!=========================== SoEvent v7r9 =====================================
! 2007-12-04 - Thomas Ruf
 - added dictionaries needed for ITrackGhostClassification tool
 - removed duplicated dictionaries
 - SoTrackCnv.cpp, removed ancient code for displaying a helix, never used as far as I know.
 - implemented user defined range for displaying tracks:
   session().setParameter('modeling.userTrackRange','true')
   session().setParameter('modeling.trackStartz','0.')
   session().setParameter('modeling.trackEndz','15000.')
!=========================== SoEvent v7r8 =====================================
! 2007-10-30 - Thomas Ruf
 - attemp to reduce amount of duplicate entry messages in dicts during startup
!=========================== SoEvent v7r7 =====================================
! 2007-09-27 - Thomas Ruf
 - added lambdab by default to MCParticle viewer
!=========================== SoEvent v7r6 =====================================
! 2007-08-06 - Thomas Ruf
 - added modeling.precision to steer precision for track displays
 - added using interpolator tool in case track has nodes, otherwise use extrapolator
 - removed making averages of states in both directions when using extrapolator
! 2007-08-03 - Thomas Ruf
 - added modeling.what option refit to TrackType in order to refit selected tracks
 - had to put things back to the dictionaries, PV relation stopped working
! 2007-07-25 - Thomas Ruf
 - cleanup of dictionaries, removed duplicated entries
!=========================== SoEvent v7r5 =====================================
! 2007-06-18 - Thomas Ruf
 - added temporarily some dicts for relations, needed to get PV associated
   to Particle
 - src/SoMCCnv.cpp, suppress error message: Do not know to display
 - modified SoParticleCnv for "better" display of particles, hopefully
 - added pattern="std::pair<int* to dict
!=========================== SoEvent v7r4 =====================================
! 2007-06-04 - Thomas Ruf
 - fixed bug in SoSTClusterConv, unable to handle mixed list of
   TT and IT clusters. Missing instances of TT AND IT det element
 - added muon property in TrackType.cpp
 - removed negative pt for backward tracks, doesn't seem to make sense
 - some minor fix in dict on vertex class
! 2007-05-24 - Thomas Ruf
 - adapted SoEventDict for a change in MCEvent, MCTrackInfo.h
! 2007-05-22 - Thomas Ruf
 - Add to TrackType support for muon tracks

! 2007-05-08 - Chris Jones
 - Add to TrackType "modeling.what" visualization options for RICH data.
   Options are :-
  + RichRecoCKRings : Draw associated CK rings as predicted by tracking
  + RichRecoPixels  : Draw associated pixels (associated due to photon
                      reconstruction)
  + RichRecoSegments : Draw associated reconstrctued radiator segments
  + RichRecoPhotons  : Draw associated reconstructed photon candidates
  + RichMCPixels     : Draw hits that are true MC associated to the selected
                       data object (e.g. due to real CK radiation)
  + RichMCSegments   : Draw associated MC radiator segments
  + RichMCPhotons    : Draw associated MC photons
  + RichMCCKRings    : Draw CK rings as from MC information
 - Update ParticleType to use TrackType for above RICH modeling options
 - Add fix to VeloClusterType to delay initialisation for DeVelo detector
   element until at late as possible (on-demand). Fixes problem with
   attempts to initialise before Detector Data is available.
 - General tidy up for SoEventSvc

! 2007-05-03 - Guy Barrand
 - SoEvent_DLD : displace Panoramix/source/MC_Viewer.cpp, Data.cpp here.
 - Introduce SoEvent_DLD DLL C++ callback library.
   Puting event model related C++ callbacks here permits to avoid
   too much "uses" in Panoramix/requirements to link the Panoramix DLL.
   Having such DLL will permit to have another strong degree of freedom
   in C++ programming callbacks, without having to pass trough the
   writing of a SoConverter or a OSC/DataAccessor. See the
   dump_header, SoEvent_exa_vis_MCParticles, MC_Viewer for examples.
 - EventType : rearrange the code to be able to handle MCHeader and ODIN
   headers.

!=========================== SoEvent v7r3 =====================================
! 2007-05-02 - Thomas Ruf
 - EventType, comment complain about missing MCHeader, confusing if reading raw data
   Still needs proper use of ODIN header in case it exists

!========================== SoEvent v7r2 =====================================
! 2007-03-19 - Florence Ranjard
 - fix the code for LHCB v22r2

! 2007-03-15 - Florence RANJARD
 - remove use of PhysDict which does not exist in PHYS_v6r1

!=========================== SoEvent v7r1 =====================================
! 2007-03-02 - Thomas Ruf.
- slight modification in SoTrackCnv for display of upstream tracks.

!============================ SoEvent v7r0 ====================================
! 2007-02-20 - Florence Ranjard
 - remove class name already set in LHCb  from  SoEvent/dict/*.h and *.xml

! 2007-02-07 - Thomas Ruf.
- and another small improvement to SoTrackCnv for rz projection.

! 2007-01-18 - Florence Ranjard
 - SoEvent is a component library: move all *.h to src, modify the code accordingly
 - fix for use with Gaudi v19r*

! 2007-01-31 - Thomas Ruf.
- fixed another bug in SoTrackCnv.
- added support for display of associated clusters of MCParticles for
  STClusters, OTClusters and MuonCoords
! 2007-01-19 - Thomas Ruf.
- fixed a bug of the bug fix, SoTrackCnv.

! 2007-01-18 - Thomas Ruf.
- fixed a bug in display of backward velo tracks, SoTrackCnv.
! 2007-01-17 - Thomas Ruf.
- add support for display of Ecal digits, muon coords associated to a MCParticle
! 2006-12-21 - Thomas Ruf.
- add support for display of daughters of composite particles via pull down menu
- add key to types of MuonCoord.
- SoTrackCnv, extrapolate long tracks up to Ecal

! 2006-12-21 - Guy Barrand
 - RecVertexType, ParticleType : revisit the addRef/release of services.
 - Type.h : have a setLocation method to commonalize the getting iterator
   for RecVertexType, ParticleType, TrackType.

! 2006-12-21 - Guy Barrand
 - revisit the addRef/release of services.

! 2006-12-21 - Thomas Ruf.
- Solved some issue with display of VeloClusters in -RZ projection which sometimes
  end up on the wrong side
! 2006-12-20 - Thomas Ruf.
- TrackType changed to start from lhcbIDs to display associate measurements
! 2006-12-19 - Guy Barrand.
- InputParticle.onx : have a create callback for the list of locations.
  (Cowork with Panoramix/source/Phys.cpp file).

! 2006-12-13 - Guy Barrand.
- SoEventSvc : addRef, release used services.

!======================= SoEvent v6r11 ====================================
! 2006-12-08 - Thomsa Ruf
 - added SoVertexCnv and VertexType temporarily before better solution found
 - modified dictionaries
!======================= SoEvent v6r10 ==================================
! 2006-12-08 - Thomsa Ruf
 - SoParticleCnv : changed TrTransporter to ParticleTransporter

! 2006-10-08 - Guy Barrand
 - SoParticleCnv, SoRecVertexCnv : rm multiNodeLimit code which is not used.
 - SoMuonCoordCnv : optimize : use SoStyleCache, have a SoIndexedLineSet to
   minimize the number of SoCoordinate3 nodes.
 - SoSTClusterCnv : optimize : use SoStyleCache, have a SoIndexedLineSet to
   minimize the number of SoCoordinate3 nodes.
 - SoOTClusterCnv : optimize : use SoStyleCache, have a SoIndexedLineSet to
   minimize the number of SoCoordinate3 nodes.
 - SoVeloClusterCnv : use the SoStyleCache of the region ; do not maintain
   a local one.

! 2006-10-06 - Guy Barrand
 - SoVeloClusterCnv : scene graph optimisation : have a common SoCoordinate3
   for all clusters and then one SoIndexedLineSet per pickable objects.
 - SoTrackCnv : comment out some line to avoid "unused variables" warnings.
 - So*Cnv::createRep : optimize the looping from the data-accessor system.
   Use the up today dummy second argument to pass the SoRegion in case
   of looping from the data-accessor system. In this case the
   getting of the currentRegion is done once, which clearly optimize things.
   See also SoEvent/Type.h SoEvent::Type<>::beginVisualize, and visualize
   methods.
   This permits to boost the visualization of the VeloCluster if manipulated
   from the data-accessor system.
 - SoVeloClusterCnv : optimize : get the tools once.

! 2006-10-05 - Guy Barrand
 - Type.h : optimize the visualize method. Mainly get the right converter
   once and call it directly in the visualize method.
 - SoVeloCLusterCnv : optimize the scene graph size by using a SoStyleCache.

! 2006-10-04 - Guy Barrand
 - Type.h : g++-3.2.3 and OSC-16 : something strange ; we loose the
   virtuality on the "safe cast" method ! Only the cast of the first
   base class is called (Lib::BaseAccessor) and not the one of OnX::BaseType.
   Sympton is that a data_visualize over MCParticle does not work ; the
   MCParticleType is not seen as an "IVisualizer".
   Have a local cast method to bypass this problem. No explanation yet.
   Is it related to the fact that we have a template in the inheritance tree ?

!========================== SoEvent v6r9 ==================================
! 2006-11-20 - Guy Barrand
 - fix Types.h for gcc 3.4

! 2006-11-16 - Thomas Ruf
  - dict/SoEventDict.h, SoEventDict.xml
  added MCTrackinfo as temporary solution to get a dictionary for this class

!=========================== SoEvent v6r8 ==================================
! 2006-10-30 - Juan PALACIOS

  - dict/SoEventDict.h, SoEventDict.xml
    . Remove entries for IDebugTool, IMCReconstructible, IVisPrimVertTool.
      These are now in Phys/PhysDict.

  - cmt/requirements
    . Increase version to v6r8
    . Remove use DaVinciMCTools
    . Use new Phys/PhysDict

! 2006-10-30 - Thomas Ruf
 - some optimization done in SoTrackCnv and SoVeloClusterCnv
! 2006-10-24 - Guy Barrand
 - prepare OSC-16.

!========================== SoEvent v6r7 ==================================
! 2006-10-11 - Thomas Ruf
 - Further improvements on SoTrackCnv
 - added temporarly some dicts in SoEventDict which will
   be replaced by a future Phys/PhysDict, needed by some python scripts
   this also required a modification to the requirements file
 - mcviewer adapted for using OnX.py in Panoramix/scripts

! 2006-10-06 - Thomas Ruf
 - MCParticle.onx: mcviewer improved, now calls python script
 - SoMCParticleCnv.cpp: tried to improve display of MCParticle trajectories and text
! 2006-09-26 - Thomas Ruf
 - src/SoTrackCnv.cpp: switch to linear extrapolator for Velo tracks,
  added visualization of state position. Simplified logic, extrapolator uses everywhere.cv
 - src/SoOTClusterCnv.cpp: tried to remove some compiler warnings
! 2006-09-22 - Pere Mato
 - dict/SoEventDict.xml: removed dictionaries for common stl classes (e.g std::char_traits<char>)

!========================== SoEvent v6r6 ===============================
! 2006-08-09 - Thomas Ruf
 - adopted to new vertex class:
   removed files: SoTrackVertexCnv.h,.cpp, SoVertexCnv.h,.cpp
   TrackVertexType.h,.cpp,VertexType.h,.cpp
   added files: SoRecVertexCnv.h,.cpp RecVertexType.h,.cpp
   changed files: requirements, SoEventDict.h,SoEventSvc.cpp,SoEvent_load.cpp
   added more track containers, Track.onx
   solved some remaining issue if subdetector info is missing: SoTrackCnv.cpp, TrackType.cpp
!========================== SoEvent v6r5 ===============================
! 2006-07-21 - Thomas Ruf
 - SoVeloClusterCnv and SoSTClusterCnv, use now position tools for displaying
   measurement trajectories. Direct cluster info does not correspond to input of fit.
 - xxxType.cpp, added key as property to some types. Helps to find objects with python.
! 2006-07-19 - Thomas Ruf
 - SoTrackCnv, solved some issue around requiring unnecessarily
  detector elements of all tracking detectors, caused problems for ACDC,
  reported by Malcolm John
- SoTrackCnv, some cleanup.
- SoTrackCnv, added visualization of chi2 at each track measurement

! 2006-07-10 - Thomas Ruf
 - added support for Particles, TrackVertex

! 2006-07-12 - Florence RANJARD
 - poca tool has been moved from TrackInterfaces to LHCbKernel
   update SoTrackCnv.cpp

!=========================== SoEvent v6r4 ===============================
! 2006-06-28 - Thomas Ruf
 - solved some issue with displaying Upstream (VeloTT) tracks
! 2006-06-27 - Thomas Ruf
 - remove obsolete method SoVeloClusterCnv:clustersPoints
 - remove obsolete class VeloPoint and VeloPointMaker
 - modified requirements accordingly

! 2006-05-29 - Thomas Ruf
 - solve some issue with plotting Velo tracks, SoTrackCnv
 - solve some issue with plotting VeloClusters,
   now correctly done using trajectories

!=========================== SoEvent v6r3 ===============================
! 2006-05-24 - Florence Ranjard
 - include SmartDataPtr.h in MuonCoordCnv.cpp
! 2006-05-22 - Thomas Ruf
 - added visualizeMCHits method in MCParticleType
 - added Onx script for flexible display of different tracks
! 2006-05-14 - Thomas Ruf
 - improved SoTrackCnv by using the Herab extrapolator between states
 - improved SoOTClusterCnv and SoTrackCnv by using trajectories
 - velo measurements are also now displayed with the track using
   modeling.what, 'VeloMeasurements'
! 2006-05-14 - Thomas Ruf
 - added some more properties to TrackType
 - modified muoncoordtype and somuoncoordcnv to compile with new software
 - also Vertextype.cpp
! 2006-05-11 - Thomas Ruf
 - fixed bug in SoTrackCnv, wrong indices of vectors leftover from CLHEP
 - added track and STCluster to SoEvent dict
! 2006-05-10 - Thomas Ruf
 - many changes to adapt to new event models and new inventor software in
   SoVeloClusterCnv, VeloClusterType, TrackType, SoTrackCnv, STClusterType
   SoSTClusterCnv, OTTimeType, SoOTClusterCnv,SoEventSvc,MCparticleType
 - removed obsolete ITClusterType, SoITClusterCnv

!=========================== SoEvent v6r2 ===============================
! 2006-05-02 - Eduardo Rodrigues
 - propagation of the renaming of the Track Event Model typedefs
   (in TrackTypes.h in Kernel/LHCbDefinitions)
 - propagation of change Track::Unique -> Track::Clone in Event/TrackEvent

!=========================== SoEvent v6r1 =================================
! 2006-03-29 - Guy Barrand
 - MCParticleType.cpp : have
    LHCb::MCVertex::DecayVertex
  instead of :
    LHCb::MCVertex::MCVertexType::DecayVertex
  Else it does not compile (with g++ 3.2.3).
 - SoMCParticleCnv.cpp : restore sizeText handling.
 - SoMCParticleCnv.cpp, SoMCCnv.cpp, SoMCHitCnv.cpp : Win : rm Warnings.

! 2006-03-24 - Thomas Ruf
 - MCParticle.h, .cpp - removed findParent, now handled directly inside
   MCParticle class. Added functionality for bcflag and time of flight
   properties.

!========================== SoEvent v6r0 =================================
! 2006-03-16 - Stefan Roiser
 - SoEventDict.h - remove ContObjSoConverter.

! 2006-03-15 - Stefan Roiser
 - SoEventDict.h - introduce ContObjSoConverter to habdle LHCb::MCHit which is not
                   a KeyedObject.

! 2006-03-14 - Florence Ranjard
 - remove from SoEventDict.h the nolonger existing include files
   prefix LHCb classes with LHCb::
 - comment out the REC and PHYS code as long as PHYS and REC are not available.

! 2006-03-09 - Guy Barrand
 - First wave of modifs to hanle LHCB_20 event model changes.
   Roughtly disconnect problematic code. At least try to recover
   MCParticle visualization...

! 2006-03-07 - Florence RANJARD
 - rm IParticleTransporter.h which belongs to PhysEvent
 - rm SoTrStoreTrackCnv, TrgTrackType, SoTrgTrackCnv, TrStateType,
   TrStoredTrackType, SoTrStateLCnv, SoTrStatePCnv.

! 2006-02-03 - Guy Barrand
- SoTrStoredTrack : fall on some OTMeasurement without "time", then crash.
  Review the code and heavily protect the various returned pointers around.
- "dynamic" -> DYNAMIC_SCENE for OSC_15.
- rm the ^M coming from some Windows user in :
  SoTrackCnv.h, TrackType.h, ITClusterType.cpp OTTimeType.cpp SoEventSvc.cpp.

-2005-12-15 - Thomas Ruf
 - added conversion for new tracks, TrackType and SoTrackCnv
 - added some more types to ITCluster and OTtime
 - minor modification to SoTrgTrackCnv to avoid crashed when no Truck to convert
-2005-12-13 - Guy Barrand
 - use the SbProjector of Vis/SoUtils (having the SoUtils namespace instead
   of the HEPVis one).
 - mv SbProjector into Vis/SoUtils.

!========================== SoEvent v5r4 ================================
! 2005-12-08 - Florence RANJARD
 - requirements - build dictionary with reflex

!=========================== SoEvent v5r3 =================================
-2005-11-14 - Thomas Ruf
 - added support for fillstream in dictionary

-2005-10-20 - Guy Barrand
 - SoEventDict in order to visualize per ContainedObject.
-2005-10-19 - Guy Barrand
 - Split Types.cpp in various <Data>Type.cpp

!=========================== SoEvent v5r2 =================================
-2005-08-25 - Jamie Tattersall
 - Types.cpp, correct Linker table for the different trgtrack types
-2005-08-22 - Thomas Ruf
 - Types.cpp, corrected bug for calculating r for a velocluster
-2005-07-25 - Thomas Ruf
 - SbProjector.cxx, SbProjector.h - added
 - requirements - modified to use SbProjector
 - all done to have signed RZ projection which does not yet exist in
   OpenScientist

!=========================== SoEvent v5r1 =================================
-2005-07-14 - Thomas Ruf
 - Types.cpp - suppress error message if MC info (Linker) is not found
 - SoTrgTrackCnv.cpp - support -RZ projection for Rtracks, correct end start
   point for tracks in RZ projection

!=========================== SoEvent v5r0 =================================
-2005-05-31 - Thomas Ruf
 - SoMCParticleCnv.cpp - added support for lineWidth option.
 - SoTrgTrackCnv.cpp - updates for new subdetector naming schemes

-2005-04-14 - Patrick Koppenburg
 - Adapt SoMCParticleCnv.cpp to CLHEP 1.9.1.2

-2005-04-12 - Florence Ranjard
 - doc - remove releasenotes.html , introduce release.notes as all other
         LHCb projects

-2005-04-12 - Olivier Callot
 - Adapt to OTCluster -> OTTime change (SoOnStoredTrack), and to the new
   DeVelo interface for zoneOfStrip and rOfStrip (SoTrgTrackCnv and Types)
 - Remove some compilation warnings in Types.cpp

-2005-04-11 - M.Tobin
 -src/VeloCluster.h, SoVeloClusterCnv.cpp - adapt to VeloDet v9 : it is not
  backward compatible.

-2005-04-11 - J.Nardulli
 -src/SoTrStoredTrackCnv.cpp - adapt code to new CLHEP and new OT Event Model

!========================== SoEvent v4r9 ==================================
-2005-03-10 - M.Witek, W.Manner
 - add SoEvent/SoTrgTrackCnv.h
 - add src/SoTrgTrackCnv.cpp, TrgVertexToContainer.cpp and .h
 - modify src/SoEventSvc.cpp, SoVeloClusterCnv.cpp, SoVertexCnv.cpp, Types.cpp

!========================== SoEvent v4r8 ===================================
-2005-02-10 - T.Ruf
 - mods in Types.cpp, SoTrStoredTrackCnv.cpp, SoMCParticleCnv.cpp

!========================== SoEvent v4r7 ===================================
-2004-10-09 - T.Ruf
 - Correction for VeloClusterCnv in order to have the strips on the sensor,
   there was a 180degree rotation too much.
   Added properties to particles (mass) and tracks (pt).
!============================================================================
