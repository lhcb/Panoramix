/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoRich_MCRichOpticalPhotonType_h
#define SoRich_MCRichOpticalPhotonType_h

// Inheritance :
#include <OnXSvc/KeyedType.h>

#include <Event/MCRichOpticalPhoton.h> //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class SoSeparator;
class MCRichOpticalPhotonRep;

class MCRichOpticalPhotonType : public OnXSvc::KeyedType<LHCb::MCRichOpticalPhoton> {
public: // Lib::IType
  using KeyedType::beginVisualize;
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );
  virtual void          beginVisualize();
  virtual void          visualize( Lib::Identifier aIdentifier, void* );
  virtual void          endVisualize();

public:
  MCRichOpticalPhotonType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc* );

private:
  SoSeparator*            m_separator;
  MCRichOpticalPhotonRep* m_rep;
  bool                    m_empty;
};

#endif
