/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file MCRichOpticalPhotonRep.h
 *
 *  Header file for visual representation object for class : MCRichOpticalPhotonRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef SORICH_MCRICHOPTICALPHOTONREP_H
#  define SORICH_MCRICHOPTICALPHOTONREP_H 1

#  include "MarkerSet.h"
#  include <Inventor/SbColor.h>

#  include <GaudiKernel/Point3DTypes.h>

class SoSeparator;
class SoStyleCache;
class SoCoordinate3;

namespace LHCb {
  class MCRichOpticalPhoton;
}

// ============================================================================
/** @class MCRichOpticalPhotonRep  MCRichOpticalPhotonRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of MCRichOpticalPhoton Object
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

class MCRichOpticalPhotonRep {

public: // help class
  class Qualities {

  public:
    Qualities( const SbColor& color, const SbColor& hcolor, const SoMarkerSet::MarkerType mType )
        : m_color( color ), m_hcolor( hcolor ), m_mType( mType ) {}

    const SbColor&          color() const { return m_color; }
    const SbColor&          hcolor() const { return m_hcolor; }
    SoMarkerSet::MarkerType markerType() const { return m_mType; }

  private:
    SbColor                 m_color;
    SbColor                 m_hcolor;
    SoMarkerSet::MarkerType m_mType;
  };

public:
  /** standard constructor
   */
  MCRichOpticalPhotonRep( const std::string& Type, const Qualities& qualities, SoStyleCache* styleCache );

  /** virtual destructor
   */
  virtual ~MCRichOpticalPhotonRep();

  /** begin a scene graph for a collection.
   *  @return the top separator.
   */
  SoSeparator* begin();

  /** represent one hit.
   *  @param hit pointer to the hit
   *  @param sep the top separator to put the sub-scene graph.
   */
  void represent( const LHCb::MCRichOpticalPhoton* digit, SoSeparator* sep );

private: // methods
  const std::string& markerStyle() const { return m_type; }
  const Qualities&   qualities() const { return m_qualities; }

  /// test if mirror reflection point is 'valid'
  inline bool mirrorPointOK( const Gaudi::XYZPoint& point ) const { return ( point.z() > 5 ); }

private: // data
  std::string m_type;

  const Qualities m_qualities;

  SoStyleCache* m_styleCache;

  SoCoordinate3* m_coordinate;
  int            m_icoord;
};

// ============================================================================
#endif ///  SORICH_MCRICHOPTICALPHOTONVIS_H
// ============================================================================
