/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file SoMCRichOpticalPhotonCnv.h
 *
 *  Header file for RICH "So" visualisation converter : SoMCRichOpticalPhotonCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef SORICH_SOMCRICHOPTICALPHOTONCNV_H
#  define SORICH_SOMCRICHOPTICALPHOTONCNV_H 1

// STD and STL
#  include <string>
#  include <vector>

// SoRich
#  include "SoRichBaseCnv.h"

/**  @class SoMCRichOpticalPhotonCnv  SoMCRichOpticalPhotonCnv.h
 *
 *   Converter for visualization of MCRichOpticalPhoton objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoMCRichOpticalPhotonCnv : public SoRichBaseCnv {
public:
  /// standard initialization method
  virtual StatusCode initialize();

  /// standard finalization  method
  virtual StatusCode finalize();

  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */, IOpaqueAddress*& /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID& classID();

  /// storage Type
  static unsigned char storageType();

  /// standard constructor
  SoMCRichOpticalPhotonCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoMCRichOpticalPhotonCnv();

private:
  /// default constructor is disabled
  SoMCRichOpticalPhotonCnv();

  /// copy constructor is disabled
  SoMCRichOpticalPhotonCnv( const SoMCRichOpticalPhotonCnv& );

  /// assignment is disabled
  SoMCRichOpticalPhotonCnv& operator=( const SoMCRichOpticalPhotonCnv& );

private:
};

// ============================================================================
#endif //  SORICH_SOMCRICHOPTICALPHOTONCNV_H
// ============================================================================
