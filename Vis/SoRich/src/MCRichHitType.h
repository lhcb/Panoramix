/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoRich_MCRichHitType_h
#define SoRich_MCRichHitType_h

// Inheritance :
#include <OnXSvc/Type.h>

#include <Event/MCRichHit.h> //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class SoSeparator;
class MCRichHitRep;

class MCRichHitType : public OnXSvc::Type<LHCb::MCRichHit> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );
  using Type<LHCb::MCRichHit>::beginVisualize;
  virtual void beginVisualize();
  virtual void visualize( Lib::Identifier aIdentifier, void* );
  virtual void endVisualize();

public:
  MCRichHitType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc* );

private:
  SoSeparator*  m_separator;
  MCRichHitRep* m_rep;
  bool          m_empty;
};

#endif
