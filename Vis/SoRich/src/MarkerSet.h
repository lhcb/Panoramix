/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SORICH_MARKERSET_H
#define SORICH_MARKERSET_H

// To easily switch from Coin3d SoMarkertSet to HEPVis_SoMarkerSet

// NOTE : the coin3d-2.5.0 SoMarkerSet does not work with gl2ps-1.22
// whilst the gl2ps production is ok with the HEPVis/SoMarkerSet.
//  G.Barrand 24/Sept/2008

//
#define SORICH_USE_HEPVIS_SOMARKER_SET

#ifdef SORICH_USE_HEPVIS_SOMARKER_SET

#  include <HEPVis/nodes/SoMarkerSet.h>
typedef HEPVis_SoMarkerSet SoMarkerSet;
#  define TRIANGLE_FILLED_5_5 TRIANGLE_UP_FILLED_5_5

#else

#  include <Inventor/nodes/SoMarkerSet.h>

#endif

#endif // SORICH_MARKERSET_H
