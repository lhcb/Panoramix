/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file SoMCRichSegmentCnv.h
 *
 *  Header file for RICH "So" visualisation converter : SoMCRichSegmentCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef SORICH_SOMCRICHSEGMENTCNV_H
#  define SORICH_SOMCRICHSEGMENTCNV_H 1

// SoRich
#  include "SoRichBaseCnv.h"

/**  @class SoMCRichSegmentCnv  SoMCRichSegmentCnv.h
 *
 *   Converter for visualization of MCRichSegment objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoMCRichSegmentCnv : public SoRichBaseCnv {
public:
  /// standard initialization method
  virtual StatusCode initialize();

  /// standard finalization  method
  virtual StatusCode finalize();

  /// Returns the representation type
  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */, IOpaqueAddress*& /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID& classID();

  /// storage Type
  static unsigned char storageType();

  SoMCRichSegmentCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoMCRichSegmentCnv();

private:
  /// default constructor is disabled
  SoMCRichSegmentCnv();

  /// copy constructor is disabled
  SoMCRichSegmentCnv( const SoMCRichSegmentCnv& );

  /// assignment is disabled
  SoMCRichSegmentCnv& operator=( const SoMCRichSegmentCnv& );

private:
};

// ============================================================================
#endif //  SORICH_SOMCRICHSEGMENTCNV_H
// ============================================================================
