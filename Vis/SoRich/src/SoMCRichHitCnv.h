/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file SoMCRichHitCnv.h
 *
 *  Header file for RICH "So" visualisation converter : SoMCRichHitCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef SORICH_SOMCRICHHITCNV_H
#  define SORICH_SOMCRICHHITCNV_H 1

// STD and STL
#  include <string>
#  include <vector>

// SoRich
#  include "SoRichBaseCnv.h"

/**  @class SoMCRichHitCnv  SoMCRichHitCnv.h
 *
 *   Converter for visualization of MCRichHit objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoMCRichHitCnv : public SoRichBaseCnv {
public:
  /// standard initialization method
  virtual StatusCode initialize();

  /// standard finalization  method
  virtual StatusCode finalize();

  /// Returns the representation type
  virtual long repSvcType() const;

  // the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */, IOpaqueAddress*& /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID& classID();

  /// storage Type
  static unsigned char storageType();

  /// standard constructor
  SoMCRichHitCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoMCRichHitCnv();

private:
  /// default constructor is disabled
  SoMCRichHitCnv();

  /// copy constructor is disabled
  SoMCRichHitCnv( const SoMCRichHitCnv& );

  /// assignment is disabled
  SoMCRichHitCnv& operator=( const SoMCRichHitCnv& );

private:
};

// ============================================================================
#endif //  SORICH_SOMCRICHHITCNV_H
// ============================================================================
