/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef OnXSvc_Helpers_h
#define OnXSvc_Helpers_h

#include <HEPVis/nodekits/SoRegion.h>
#include <OnX/Helpers/Inventor.h>

//////////////////////////////////////////////////////////////////////////////
inline SoRegion* page_currentRegion( SoPage& aPage )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return aPage.currentRegion();
}

#define DYNAMIC_SCENE "dynamicScene"
#define STATIC_SCENE "staticScene"

//////////////////////////////////////////////////////////////////////////////
inline void region_addToDynamicScene( SoRegion& aRegion, SoNode* aNode )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aRegion.doIt( SbAddNode( aNode, DYNAMIC_SCENE ) );
}
//////////////////////////////////////////////////////////////////////////////
inline void region_addToStaticScene( SoRegion& aRegion, SoNode* aNode )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aRegion.doIt( SbAddNode( aNode, STATIC_SCENE ) );
}

#endif
