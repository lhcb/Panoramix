/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef OnXSvc_ISvcLocatorManager_h
#define OnXSvc_ISvcLocatorManager_h

class ISvcLocator;
class IService;

class ISvcLocatorManager {
public:
  virtual ~ISvcLocatorManager() {}
  virtual ISvcLocator* serviceLocator() const              = 0;
  virtual IService*    service( const std::string& ) const = 0;
};

#endif
