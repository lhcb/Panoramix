/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef OnXSvc_MetaType_h
#define OnXSvc_MetaType_h

class IIntrospectionSvc;

#include <Lib/BaseType.h>

class MetaType : public Lib::BaseType {
public:
  MetaType( IPrinter&, IIntrospectionSvc* );

public: // Lib::IType
  virtual const std::string& name() const;
  virtual bool               setName( const std::string& );
  virtual void               setIterator( Lib::IIterator* );
  virtual Lib::IIterator*    iterator();
  virtual Lib::Property*     getProperties( int& );
  virtual Lib::Variable      value( Lib::Identifier, const std::string& );

private:
  std::string        fType;
  IIntrospectionSvc* fIntrospectionSvc;
  Lib::IIterator*    fIterator;
};

#endif
