/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ISoConversionSvc_h
#define ISoConversionSvc_h

#include <GaudiKernel/IConversionSvc.h>

static const InterfaceID IID_ISoConversionSvc( 91, 1, 0 );

class ISoConversionSvc : virtual public IConversionSvc {
public:
  virtual ~ISoConversionSvc() {}

public:
  static const InterfaceID& interfaceID() { return IID_ISoConversionSvc; }
};

#endif
