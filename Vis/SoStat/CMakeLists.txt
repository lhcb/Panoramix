###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: SoStat
################################################################################
gaudi_subdir(SoStat v6r8)

gaudi_depends_on_subdirs(GaudiKernel
                         Vis/OnXSvc
                         Vis/RootSvc)

find_package(AIDA)
find_package(OpenScientist REQUIRED COMPONENTS Vis)
include_directories(SYSTEM ${OPENSCIENTIST_INCLUDE_DIRS})

gaudi_add_module(SoStat
                 src/SoStatDLL.cpp
                 src/SoStatSvc.cpp
                 src/SoHistogramCnv.cpp
                 INCLUDE_DIRS SYSTEM OpenScientist Vis/OnXSvc Vis/RootSvc AIDA GaudiKernel
                 LINK_LIBRARIES OpenScientist GaudiKernel)


string(REPLACE "-Wsuggest-override" " " CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})

