!-----------------------------------------------------------------------------
! Package     : Vis/SoStat
! Responsible : Thomas Ruf
! Purpose     : 
!-----------------------------------------------------------------------------

!======================= SoStat v6r8 ===================================
! 2016-01-19 - Thomas Ruf

!======================= SoStat v6r7 ===================================
! 2014-01-28 - Marco Clemencic
 - Added CMake configuration file.

!======================= SoStat v6r6 ===================================

! 2009-09-29 - Guy Barrand
 - remove the logic to attempt to use TCanvas embeded in the GUI.
   But now we look for the ISession plotting.plotter resource
   and in case of "TCanvas" asks to the RootSvc to get
   the TH histo and do a TH.Draw(). This, with coworking modifs
   in RootSvc, should permit to pick an histo in the /stat and
   have it plotted by using a SoPlotter or a TCanvas.

! 2009-09-24 - Chris Jones
 - Improvements to messaging levels.

!======================= SoStat v6r5 ===================================

! 2009-06-015 - Guy Barrand
 - SoHistogramCnv : comment out ROOT related things (since we want to
   deprecate the Vis/RootSvc).
 - SoHistogramCnv : Handle the OnXLab_tree.append session option so that,
   by default, clicking on a stat histo first clear the region.

!======================= SoStat v6r4 ===================================

! 2009-06-03 - Chris Jones
 - Fix compilation warnings with gcc 4.3
 - Remove obsolete SoStat_load.cpp file

! 2009-05-21 - Guy Barrand
 - SoHistogramCnv::createRep : handle PlanePageViewer widget.

!======================= SoStat v6r3 ===================================

! 2008-02-22 - Guy Barrand
 - SoHistogramCnv : rm OSC_VERSION_16_0 code.

!======================= SoStat v6r2 ===================================
! 2008-02-18 - Thomas Ruf
 - SoHistogramCnv : int isValid changed to bool isValid 

!======================= SoStat v6r1 ===================================
! 2007-03-12 - Guy Barrand
 - SoHistogramCnv : load RootSvc only if needed. Else mess at the 
   level of Panoramix job options with the startup of ROOT.

!======================= SoStat v6r0 ======================================
! 2007-02-02 - Florence RANJARD
 - move *.h from SoStat to src
 - use macro DECLARE_XXX_FACTORY to use new PluginManager

!======================= SoStat v5r2 =======================================
! 2005-05-15 - Guy Barrand
 - SoHistogramCnv.cpp : handle the case where the current widget is a TCanvas.
   Done through the OnX IWidget and IRootSvc (to avoid "hard" dependencies 
   toward OnX and ROOT). Need the IUserInterfaceSvc::currentWidget method.

!======================== SoStat v5r1 ====================================
! 2005-03-29 - Guy Barrand
 - SoHistogramCnv.cpp : have the OSC_15 one. Rm SoHistogramCnv.cpp_OSC_12.

!========================= SoStat v5r0 ====================================
! 2005-01-05 - Guy Barrand
 - Prepare OSC_15. Do things in order to be still compatible with OSC_12.

!========================= SoStat v4r5 ====================================
! 2005-04-15 - Florence RANJARD
 - replace releasenotes.html with standard LHCb release.notes

!==========================================================================

