###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: SoDet
################################################################################
gaudi_subdir(SoDet v7r18)

gaudi_depends_on_subdirs(Det/DetDesc
                         Vis/OnXSvc)

find_package(OpenScientist REQUIRED COMPONENTS Vis)
include_directories(SYSTEM ${OPENSCIENTIST_INCLUDE_DIRS})

if(GAUDI_HIDE_WARNINGS)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-overloaded-virtual")
endif()

gaudi_add_module(SoDet
                 src/SoDetDLL.cpp
                 src/SoDetSvc.cpp
                 src/Types.cpp
                 src/SoDetConverter.cpp
                 src/SoDetElemCnv.cpp
                 src/SoLVolumeCnv.cpp
                 INCLUDE_DIRS Vis/OnXSvc OpenScientist
                 LINK_LIBRARIES DetDescLib OpenScientist)

string(REPLACE "-Wsuggest-override" " " CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS}) 

