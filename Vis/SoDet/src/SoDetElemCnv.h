/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoDet_SoDetElemCnv_h
#define SoDet_SoDetElemCnv_h

#include "GaudiKernel/MsgStream.h"
#include <memory>

#include "SoDetConverter.h"

class SoDetElemCnv : public SoDetConverter {
public:
  SoDetElemCnv( ISvcLocator* );
  virtual StatusCode createRep( DataObject*, IOpaqueAddress*& );

public:
  static const CLID&   classID();
  static unsigned char storageType();

private:
  mutable std::unique_ptr<MsgStream> fLog;

private:
  /// On-demand access to MsgStream object
  inline MsgStream& msgStream() const {
    if ( !fLog ) { fLog.reset( new MsgStream( msgSvc(), "SoDetElemCnv" ) ); }
    return *fLog;
  }
};

#endif
