/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RootSvc_RootSvc_h
#define RootSvc_RootSvc_h

// Inheritance :
#include <GaudiKernel/Service.h>
#include <RootSvc/IRootSvc.h>

class RootSvc : public Service, virtual public IRootSvc {
public: // IInterface
  virtual StatusCode queryInterface( const InterfaceID&, void** );

public: // IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();

public: // IRootSvc
  virtual StatusCode visualize( const AIDA::IHistogram& );
  // protected:
  RootSvc( const std::string&, ISvcLocator* );
  virtual ~RootSvc();
};

#endif
