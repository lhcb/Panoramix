/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IRootSvc_h
#define IRootSvc_h

// Inheritance :
#include <GaudiKernel/IService.h>

static const InterfaceID IID_IRootSvc( 346, 1, 0 );

namespace AIDA {
  class IHistogram;
}

class IRootSvc : virtual public IService {
public:
  virtual ~IRootSvc() {}

  static const InterfaceID& interfaceID() { return IID_IRootSvc; }

  virtual StatusCode visualize( const AIDA::IHistogram& ) = 0;
};

#endif
