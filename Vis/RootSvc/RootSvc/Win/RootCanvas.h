/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RootSvc_WinTCanvas_h
#define RootSvc_WinTCanvas_h

// Inheritance :
#include <OnX/Win/WinTk.h>

class TCanvas;

namespace WinTk {

  class RootCanvas : public Component {
  public:
    static void startTimer();
    static void stopTimer();

  public:
    RootCanvas( Component&, const std::string&, const std::string& = "" );
    virtual ~RootCanvas();
    // virtual void show();
    TCanvas* canvas() const;
    void     clear();
    void     update();

  private:
    static LRESULT CALLBACK proc( HWND, UINT, WPARAM, LPARAM );
    static void CALLBACK timerProc( HWND, UINT, UINT, DWORD );

  private:
    int      fWID;
    TCanvas* fCanvas;
  };

} // namespace WinTk

#endif
