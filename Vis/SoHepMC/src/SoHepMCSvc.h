/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoHepMC_SoHepMCSvc_h
#define SoHepMC_SoHepMCSvc_h

// Gaudi :
#include "Kernel/IParticlePropertySvc.h"
#include <GaudiKernel/Service.h>

class IUserInterfaceSvc;
class IDataProviderSvc;
class IParticlePropertySvc;
class IToolSvc;

class SoHepMCSvc : public Service {
public: // IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();

  SoHepMCSvc( const std::string&, ISvcLocator* );
  virtual ~SoHepMCSvc();

protected:
  IUserInterfaceSvc*          m_uiSvc;
  IDataProviderSvc*           m_eventDataSvc;
  LHCb::IParticlePropertySvc* m_particlePropertySvc;
};

#endif
