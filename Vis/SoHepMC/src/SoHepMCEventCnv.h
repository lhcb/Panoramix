/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoHepMC_SoHepMCEventCnv_h
#define SoHepMC_SoHepMCEventCnv_h

#include "Kernel/IParticlePropertySvc.h"
#include <GaudiKernel/Converter.h>

class ISoConversionSvc;
class IUserInterfaceSvc;
class IParticlePropertySvc;

class SoHepMCEventCnv : public Converter {
public:
  SoHepMCEventCnv( ISvcLocator* );
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual long       repSvcType() const;
  virtual StatusCode createRep( DataObject*, IOpaqueAddress*& );

public:
  static const CLID&   classID();
  static unsigned char storageType();

private:
  ISoConversionSvc*           fSoCnvSvc;
  IUserInterfaceSvc*          fUISvc;
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
