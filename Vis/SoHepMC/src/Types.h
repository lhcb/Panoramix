/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoHepMC_Types_h
#define SoHepMC_Types_h
#include "Kernel/IParticlePropertySvc.h"

class IParticlePropertySvc;
class IDataProviderSvc;
class IUserInterfaceSvc;

class SoRegion;

namespace HepMC {
  class GenParticle;
}

#include <Lib/Interfaces/IIterator.h>
#include <Lib/Interfaces/ISession.h>
#include <OnX/Core/BaseType.h>

class GenParticleType : public OnX::BaseType {
public: // Lib::IType
  virtual std::string     name() const;
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable   value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  GenParticleType( LHCb::IParticlePropertySvc*, IDataProviderSvc*, IUserInterfaceSvc* );
  static void represent( HepMC::GenParticle*, ISession*, SoRegion*, LHCb::IParticlePropertySvc* );
  static void representDecay( HepMC::GenParticle*, ISession*, SoRegion*, LHCb::IParticlePropertySvc* );

private:
  void clear();

private:
  std::string                 fType;
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
  IDataProviderSvc*           fDataProviderSvc;
  IUserInterfaceSvc*          fUISvc;
};

#endif
