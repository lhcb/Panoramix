/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#define SOCALO_CALOCLUSTERREP_CPP 1
// ============================================================================
// ============================================================================
// CLHEP
// ============================================================================
#include "CLHEP/Geometry/Point3D.h"
// From GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
// ============================================================================
// Inventor
// ============================================================================
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoRotation.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoTranslation.h>
// ============================================================================
#include <HEPVis/nodes/SoArrow.h>
#include <HEPVis/nodes/SoEllipsoid.h>
#include <HEPVis/nodes/SoEllipticalPrism.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoTextTTF.h>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/SmartRef.h"
// ============================================================================
// DetDesc
// ============================================================================
#include "DetDesc/IGeometryInfo.h"
// ============================================================================
// CaloEvent
// ============================================================================
#include "Event/CaloCluster.h"
#include "Event/CaloDataFunctor.h"
// ============================================================================
// CaloDet
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
// ============================================================================
// SoUtils
// ============================================================================
#include "SoUtils/SoUtils.h"
// ============================================================================
// local
// ============================================================================
#include "CaloClusterRep.h"
// ============================================================================

// ============================================================================
/** @file CaloClusterRep.cpp
 *
 *  implementation of CaloCluster representation
 *
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 *  @date   20/10/2001
 */
// ============================================================================

// ============================================================================
/** standard constructor
 *  @param DeCalo         pointer to DeCalorimeter object
 *                        (source of geometry  information)
 *  @param Type           Own "Object Type" for picking
 *  @param TypeD          Digit "Object Type" for picking
 *  @param EScale         drawing scale
 *  @param EtVis          flag for drawing the transverse energy
 *  @param LogVis         flag for logarithmic drawing
 *  @param TxtVis         flag for drawing the text
 *  @param TxtSize        text size
 *  @param TxtPos         text position
 *  @param VisDigits      flag for drawing of digits
 *  @param VisShared      flag for special processing of shared digits
 *  @param VisStructure   flag for visualization of cluster structure
 *  @param VisCovariance  flag for visualization of covarinace matrix
 *  @param VisSpread      flag for visualization of cluster spread
 *  @param VisPrism       flag for visualization of spread prism
 */
// ============================================================================
CaloClusterRep::CaloClusterRep( const DeCalorimeter* DeCalo, const std::string& Type, const std::string& TypeD,
                                const double EScale, const bool EtVis, const bool LogVis, const bool TxtVis,
                                const double TxtSize, const double TxtPos, const bool VisDigits, const bool VisShared,
                                const bool VisStructure, const bool VisCovariance, const bool VisSpread,
                                const bool VisPrism )
    : m_type( Type )
    , m_visDigits( VisDigits )
    , m_visShared( VisShared )
    , m_visStructure( VisStructure )
    , m_visCovariance( VisCovariance )
    , m_visSpread( VisSpread )
    , m_visPrism( VisPrism )
    , m_digRep( DeCalo, TypeD, EScale, EtVis, LogVis, TxtVis, TxtSize, TxtPos ) {
  if ( visShared() ) { setVisDigits( true ); }
  if ( visPrism() ) { setVisSpread( true ); }
}

// ============================================================================
/// destructor
// ============================================================================
CaloClusterRep::~CaloClusterRep() {}

// ============================================================================
/// the only one essential method
// ============================================================================
SoSeparator* CaloClusterRep::operator()( const LHCb::CaloCluster* cluster ) const {
  /// if detector is NULL or cluster is NULL drawing is not possible!
  if ( 0 == deCalo() || 0 == cluster ) { return 0; }
  /// create separator
  SoSeparator* separator = new SoSeparator();
  ///
  // Build picking string id :
  std::string sid;
  if ( !type().empty() ) {
    char* s = new char[type().size() + 32];
    ::sprintf( s, "%s/0x%lx", m_type.c_str(), (unsigned long)cluster );
    sid = std::string( s );
    delete[] s;
  }
  /// visualize the digits?
  unsigned nUsedDigs = 0;
  for ( auto it = cluster->entries().begin(); cluster->entries().end() != it; ++it ) {
    const LHCb::CaloDigit* digit = it->digit();
    if ( 0 == digit ) { continue; }
    const LHCb::CaloDigitStatus::Status& status = it->status();
    /// create the representation of the digit
    const Gaudi::XYZPoint& cellCenter = deCalo()->cellCenter( digit->cellID() );
    const double           cellsize   = deCalo()->cellSize( digit->cellID() );
    ///
    ++nUsedDigs;
    ///
    if ( visDigits() ) {
      SoSeparator* sep = digRep()( digit, sid );
      if ( 0 == sep ) { continue; }
      separator->addChild( sep );

      if ( visShared() && status == LHCb::CaloDigitStatus::Mask::SharedCell ) {
        const double     height = digRep().size() * it->fraction();
        SoArrow*         arrow  = new SoArrow();
        Gaudi::XYZVector vect =
            Gaudi::XYZPoint( cluster->position().x(), cluster->position().y(), cluster->position().z() ) - cellCenter;

        vect.SetZ( 0 );                    // SetZ in DisplacementVector3D - setZ in CLHEP !!!!!
        vect *= 0.5 * cellsize / vect.r(); // R() = sqrt(mag2)
        ///
        const Gaudi::XYZPoint tip( cellCenter + vect );
        ///
        arrow->tail.setValue( (float)cellCenter.x(), (float)cellCenter.y(),
                              (float)( cluster->position().z() + height ) );
        arrow->tip.setValue( (float)tip.x(), (float)tip.y(), (float)( cluster->position().z() + height ) );
        ///
        SoSceneGraph* sep = new SoSceneGraph;
        sep->setString( sid.c_str() );
        sep->addChild( arrow );
        //
        separator->addChild( sep );
      }
    }
  }
  ///
  if ( visStructure() ) {
    Gaudi::XYZPoint seedPoint( cluster->position().x(), cluster->position().y(), cluster->position().z() );
    for ( auto it = cluster->entries().begin(); cluster->entries().end() != it; ++it ) {
      const LHCb::CaloDigit*               digit  = it->digit();
      const LHCb::CaloDigitStatus::Status& status = it->status();
      if ( 0 == digit ) { continue; }
      ///
      const Gaudi::XYZPoint& cellCenter = deCalo()->cellCenter( digit->cellID() );
      const double           cellsize   = deCalo()->cellSize( digit->cellID() );
      /// redefine the seed point
      if ( status == LHCb::CaloDigitStatus::Mask::SeedCell ) { seedPoint = cellCenter; }
      ///
      Gaudi::XYZVector vect = seedPoint - cellCenter;
      if ( 0 != vect.mag2() ) {
        vect *= 0.25 * cellsize / vect.r();
        ///
        const Gaudi::XYZPoint tip( cellCenter + vect );
        ///
        SoArrow* arrow = new SoArrow();
        ///
        arrow->tail.setValue( (float)cellCenter.x(), (float)cellCenter.y(), (float)cluster->position().z() );
        arrow->tip.setValue( (float)tip.x(), (float)tip.y(), (float)cluster->position().z() );
        ///
        SoSceneGraph* sep = new SoSceneGraph;
        sep->setString( sid.c_str() );
        sep->addChild( arrow );
        //
        separator->addChild( sep );
      }
    }
  }
  /// visualize the ellipses
  /// (1) the "cov" ellipsoid !!!
  if ( visCovariance() ) {
    Gaudi::SymMatrix3x3 mtrx;
    /// copy the matrix
    ///        x   x                                     x   x
    mtrx( 0, 0 ) = cluster->position().covariance()( LHCb::CaloPosition::X, LHCb::CaloPosition::X );
    ///        y   x                                     y   x
    mtrx( 1, 0 ) = cluster->position().covariance()( LHCb::CaloPosition::Y, LHCb::CaloPosition::X );
    ///        y   y                                     y   y
    mtrx( 1, 1 ) = cluster->position().covariance()( LHCb::CaloPosition::Y, LHCb::CaloPosition::Y );
    ///        e   x                                     x   e
    mtrx( 2, 0 ) = cluster->position().covariance()( LHCb::CaloPosition::X, LHCb::CaloPosition::E );
    ///        e   y                                     y   e
    mtrx( 2, 1 ) = cluster->position().covariance()( LHCb::CaloPosition::Y, LHCb::CaloPosition::E );
    ///        e   e                                     e   e
    mtrx( 2, 2 ) = cluster->position().covariance()( LHCb::CaloPosition::E, LHCb::CaloPosition::E );
    ///
    double height = cluster->e();
    /// et ?
    if ( etVis() ) {
      const double sintheta =
          sin( Gaudi::XYZPoint( cluster->position().x(), cluster->position().y(), cluster->position().z() ).theta() );
      height = cluster->e() * sintheta;
      ///
      mtrx( 2, 0 ) *= sintheta;
      mtrx( 2, 1 ) *= sintheta;
      mtrx( 2, 2 ) *= sintheta;
      mtrx( 2, 2 ) *= sintheta;
    }
    /// log ?
    if ( logVis() ) {
      mtrx( 2, 0 ) /= -1.0 * height;
      mtrx( 2, 1 ) /= -1.0 * height;
      mtrx( 2, 2 ) /= -1.0 * height;
      mtrx( 2, 2 ) /= -1.0 * height;
    }
    /// rescale
    mtrx( 2, 0 ) *= eScale();
    mtrx( 2, 1 ) *= eScale();
    mtrx( 2, 2 ) *= eScale();
    mtrx( 2, 2 ) *= eScale();
    ///
    SoEllipsoid* ell = 0;
    StatusCode   sc  = SoUtils::ellipsoid(
        Gaudi::XYZPoint( cluster->position().x(), cluster->position().y(), cluster->position().z() ), mtrx, ell );
    ///
    if ( sc.isSuccess() && 0 != ell ) {
      SoSceneGraph* sep = new SoSceneGraph;
      sep->setString( sid.c_str() );
      sep->addChild( ell );
      separator->addChild( sep );
    }
    ///
  }
  /// (2) the "spread" elliptical prism
  if ( visSpread() ) {
    ///
    SoEllipticalPrism* ell = 0;
    ///
    double height = 1.;
    if ( visPrism() ) {
      height = cluster->e();
      if ( etVis() ) {
        const double sintheta =
            sin( Gaudi::XYZPoint( cluster->position().x(), cluster->position().y(), cluster->position().z() ).theta() );
        height = cluster->e() * sintheta;
      }
      if ( logVis() ) { height = 0 < height ? log10( height ) : 1.0; }
      ///
      height *= eScale();
      if ( !visDigits() ) { height /= 2.; }
      ///
    }
    ///
    StatusCode sc = SoUtils::ellipticalprism(
        Gaudi::XYZPoint( cluster->position().x(), cluster->position().y(), cluster->position().z() + height / 2 ),
        cluster->position().spread(), height / 2, ell );
    ///
    if ( sc.isSuccess() && 0 != ell ) {
      SoSceneGraph* sep = new SoSceneGraph;
      sep->setString( sid.c_str() );
      sep->addChild( ell );
      separator->addChild( sep );
    }
    ///
  }
  ///
  return separator;
  ///
}
