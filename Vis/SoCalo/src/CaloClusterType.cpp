/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// include files
// OnXSvc
#include "OnXSvc/Helpers.h"
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"
// OnX
#include <OnX/Interfaces/IUI.h>
// this
#include "CaloClusterType.h"
// Inventor
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoSeparator.h>
// HEPVis :
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
// STD & STL
#include <algorithm>
// GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
// Lib
#include <Lib/Compiler.h>
#include <Lib/Interfaces/IIterator.h>
#include <Lib/Interfaces/IManager.h>
#include <Lib/Interfaces/ISession.h>
#include <Lib/Property.h>
#include <Lib/Variable.h>
#include <Lib/smanip.h>
// Gaudi
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartRef.h"
#include <GaudiKernel/ISvcLocator.h>
// DetDesc
#include "DetDesc/IGeometryInfo.h"
// CaloEvent
#include "Event/CaloDataFunctor.h"
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// local
#include "CaloClusterRep.h"

#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>
//

// ============================================================================
/** @file CaloClusterType.cpp
 *
 * Implementation file for class CaloClusterType
 *
 * @date 10/10/2001
 * @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================

// ============================================================================
/** Standard Constructor
 *  @param Type    "So"-type name
 *  @param SvcLoc  pointer to Service Locator
 */
// ============================================================================
CaloClusterType::CaloClusterType( const std::string& Type, ISvcLocator* SvcLoc, IPrinter& Printer )
    : OnX::BaseType( Printer ), CaloBaseType( Type, SvcLoc ) {
  initialize().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  addProperty( "id", Lib::Property::POINTER );
  addProperty( "e", Lib::Property::DOUBLE );
  addProperty( "x", Lib::Property::DOUBLE );
  addProperty( "y", Lib::Property::DOUBLE );
  addProperty( "digs", Lib::Property::INTEGER );
  addProperty( "status", Lib::Property::INTEGER );
  /// transverse energy
  addProperty( "et", Lib::Property::DOUBLE );
  /// at boundary ?
  addProperty( "boundary", Lib::Property::BOOLEAN );
  /// seed cell cellID
  addProperty( "cellID", Lib::Property::INTEGER );
  /// seed cell cellsize
  addProperty( "cellsize", Lib::Property::DOUBLE );
}

// ============================================================================
/** Destructor
 */
// ============================================================================
CaloClusterType::~CaloClusterType() { finalize().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }

// ===========================================================================
/** get name(type)
 */
// ============================================================================
std::string CaloClusterType::name() const { return CaloBaseType::name(); }

// ===========================================================================
// get Lib identifiers
// ============================================================================
Lib::IIterator* CaloClusterType::iterator() {
  class Iterator : public Lib::IIterator {
  public: // Lib::IIterator
    virtual Lib::Identifier object() {
      if ( !fVector ) return 0;
      while ( true ) {
        if ( fIterator == fVector->end() ) return 0;
        if ( *fIterator ) return *fIterator;
        ++fIterator;
      }
      return 0;
    }
    virtual void next() {
      if ( fVector ) ++fIterator;
    }
    virtual void* tag() { return 0; }

  public:
    Iterator( LHCb::CaloClusters* aVector = 0 ) : fVector( aVector ) {
      if ( fVector ) fIterator = fVector->begin();
    }

  private:
    LHCb::CaloClusters*          fVector;
    LHCb::CaloClusters::iterator fIterator;
  };

  if ( 0 == evtSvc() ) {
    Error( "iterator(): evtSvc() points to NULL!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return 0;
  }
  /// get address of objects
  setData( attribute( name() ) );
  Print( "Data Container '" + data() + "' is used", MSG::DEBUG )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  if ( data().empty() ) {
    Warning( "iterator(): '" + name() + "' attribute is not given!" )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return 0;
  }
  /// retrieve data container
  SmartDataPtr<LHCb::CaloClusters> clusters( evtSvc(), data() );
  if ( !clusters ) {
    Error( "iterator():: could not retrieve '" + data() + "'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return 0;
  }
  if ( clusters->size() == 0 ) {
    Warning( "iterator(): '" + name() + "' collection is empty." )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return new Iterator();
  }

  return new Iterator( clusters ); // deleted by the caller.
}

// ===========================================================================
/// get the specific property
// ============================================================================
Lib::Variable CaloClusterType::value( Lib::Identifier identifier, const std::string& tag, void* ) {
  /// local type
  if ( tag.empty() ) {
    Error( "getProperty() - wrong name!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return Lib::Variable( printer() );
  }
  /// bad style :-(((
  const LHCb::CaloCluster* cluster = (const LHCb::CaloCluster*)identifier;
  if ( !cluster ) {
    Error( "getProperty('" + tag + "' LHCb::CaloCluster* points to NULL!" )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return Lib::Variable( printer() );
  }
  ///
  if ( "id" == tag ) {
    return Lib::Variable( printer(), (void*)cluster );
  } else if ( "e" == tag ) {
    return Lib::Variable( printer(), (double)cluster->e() );
  } else if ( "x" == tag ) {
    return Lib::Variable( printer(), (double)cluster->position().x() );
  } else if ( "y" == tag ) {
    return Lib::Variable( printer(), (double)cluster->position().y() );
  } else if ( "digs" == tag ) {
    return Lib::Variable( printer(), (int)cluster->entries().size() );
  } else if ( "status" == tag ) {
    return Lib::Variable( printer(), (int)cluster->type() );
  }

  const DeCalorimeter* decalo = calo( LHCb::Detector::Calo::CellCode::CaloNumFromName( myType() ) );

  if ( "et" == tag ) {
    if ( 0 == decalo ) { return Lib::Variable( printer(), (double)-1.0 ); }
    const double et = cluster->e() * sin( Gaudi::XYZPoint( cluster->position().x(), cluster->position().y(),
                                                           decalo->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) ).z() )
                                              .theta() );
    return Lib::Variable( printer(), et );
  } else if ( "boundary" == tag ) {
    for ( auto it1 = cluster->entries().begin(); cluster->entries().end() != it1; ++it1 ) {
      const LHCb::CaloDigit* dig1 = it1->digit();
      if ( !dig1 ) { continue; }
      for ( auto it2 = it1 + 1; cluster->entries().end() != it2; ++it2 ) {
        const LHCb::CaloDigit* dig2 = it2->digit();
        if ( !dig2 ) { continue; }
        if ( dig1->cellID().area() != dig2->cellID().area() ) { return Lib::Variable( printer(), true ); }
      }
    }
    return Lib::Variable( printer(), false );
  }
  /// look for seeding digit
  const LHCb::CaloDigit* seed = nullptr;
  for ( auto it = cluster->entries().begin(); cluster->entries().end() != it; ++it ) {
    const LHCb::CaloDigit* dig = it->digit();
    if ( 0 == dig ) { continue; }
    if ( it->status() == LHCb::CaloDigitStatus::Mask::SeedCell ) {
      seed = dig;
      break;
    }
  }
  ///
  if ( "cellID" == tag ) {
    int cellID = 0 != seed ? (int)seed->cellID().all() : 0;
    return Lib::Variable( printer(), cellID );
  } else if ( "cellsize" == tag ) {
    const DeCalorimeter* decalo   = 0 != seed ? calo( seed->cellID().calo() ) : 0;
    const double         cellsize = ( ( 0 != decalo ) && ( 0 != seed ) ) ? decalo->cellSize( seed->cellID() ) : -1;
    return Lib::Variable( printer(), cellsize );
  }
  ///
  Error( "getProperty(): unknown name='" + tag + "'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  ///
  return Lib::Variable( printer() );
}

// ===========================================================================
// ============================================================================
void CaloClusterType::beginVisualize() { m_parentNode = parent( DYNAMIC_SCENE ); }
void CaloClusterType::endVisualize() { m_parentNode = 0; }
void CaloClusterType::visualize( Lib::Identifier id, void* ) {
  const LHCb::CaloCluster* cluster = (const LHCb::CaloCluster*)id;
  if ( 0 == cluster ) {
    Error( "visualize():  LHCb::CaloCluster* points to NULL!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }
  ///
  std::string value;
  uiSvc()->session()->parameterValue( "modeling.what", value );
  if ( value == "MCParticle" ) {
    LHCb::CaloCluster* object = (LHCb::CaloCluster*)id;
    visualizeMCParticle( *object );
    return;
  }
  SoSeparator* node = m_parentNode;
  if ( node == 0 ) {
    Error( "visualize(): parent SoSeparator* points to NULL! " )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }
  /// create and configure visualizator
  const DeCalorimeter* Calo = calo( LHCb::Detector::Calo::CellCode::CaloNumFromName( myType() ) );
  if ( 0 == Calo ) {
    Error( "visualize(): DeCalorimeter* points to NULL! " ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }
  ///
  CaloClusterRep vis( Calo, myType() );
  ///
  /// E or Et ?
  {
    bool etVis = false;
    Lib::smanip::tobool( attribute( "CaloEtVis" ), etVis );
    vis.setEtVis( etVis );
  }
  /// Linear or logarithm ?
  {
    bool logVis = false;
    Lib::smanip::tobool( attribute( "CaloLogVis" ), logVis );
    vis.setLogVis( logVis );
  }
  /// Scale
  {
    double scale = 5;
    Lib::smanip::todouble( attribute( "CaloScale" ), scale );
    scale *= Gaudi::Units::cm / Gaudi::Units::GeV;
    vis.setEScale( scale );
  }
  /// visualize the digits?
  {
    bool visDigits = true;
    Lib::smanip::tobool( attribute( "CaloVisDigits" ), visDigits );
    vis.setVisDigits( visDigits );
  }
  /// visualize shared digits ?
  {
    bool visShared = false;
    Lib::smanip::tobool( attribute( "CaloVisShared" ), visShared );
    vis.setVisShared( visShared );
  }
  /// visualize the structure ?
  {
    bool visStructure = false;
    Lib::smanip::tobool( attribute( "CaloVisStructure" ), visStructure );
    vis.setVisStructure( visStructure );
  }
  /// visualize the covariance ?
  {
    bool visCovariance = false;
    Lib::smanip::tobool( attribute( "CaloVisCovariance" ), visCovariance );
    vis.setVisCovariance( visCovariance );
  }
  /// visualize the cluster spread ?
  {
    bool visSpread = false;
    Lib::smanip::tobool( attribute( "CaloVisSpread" ), visSpread );
    vis.setVisSpread( visSpread );
  }
  /// visualize the spread prism ?
  {
    bool visPrism = false;
    Lib::smanip::tobool( attribute( "CaloVisPrism" ), visPrism );
    vis.setVisPrism( visPrism );
  }

  bool found = false;
  for ( LHCb::CaloCluster::Entries::const_iterator it = cluster->entries().begin(); cluster->entries().end() != it;
        ++it ) {
    const LHCb::CaloDigit* digit = it->digit();
    if ( digit ) {
      found = true;
      break;
    }
  }
  if ( !found ) {
    Error( "visualize(): cluster with no valid digits ! " ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb( attribute( "modeling.color" ), r, g, b );
  Lib::smanip::torgb( attribute( "modeling.highlightColor" ), hr, hg, hb );
  bool modeling = modelingSolid();
  vis.setSolid( modeling );

  /// create the representation
  SoSeparator* separator = vis( cluster );
  if ( 0 == separator ) {
    Error( "visualize(): could not create SoSeparator!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }

  SoMaterial* highlightMaterial =
      styleCache()->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ), float( hg ), float( hb ) );
  separator->insertChild( highlightMaterial, 0 );
  if ( modeling ) {
    separator->insertChild( styleCache()->getLightModelPhong(), 0 );
  } else { // SoLightModel::BASE_COLOR);
    separator->insertChild( styleCache()->getLightModelBaseColor(), 0 );
  }

  node->addChild( separator );
}
//////////////////////////////////////////////////////////////////////////////
void CaloClusterType::visualizeMCParticle( LHCb::CaloCluster& aCaloCluster )
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LinkedTo<LHCb::MCParticle, LHCb::CaloCluster> CaloClusterLink( evtSvc(), 0, LHCb::CaloClusterLocation::Ecal );
  LHCb::MCParticle*                             part = CaloClusterLink.first( &aCaloCluster );
  const std::string                             tmp( "SoConversionSvc" );
  ISoConversionSvc*                             soSvc;
  svcLoc()->service( tmp, soSvc ).ignore();

  while ( NULL != part ) {
    LHCb::MCParticles* objs = new LHCb::MCParticles;
    objs->add( part );
    // Convert it :
    IOpaqueAddress* addr = 0;
    StatusCode      sc   = soSvc->createRep( objs, addr );
    if ( sc.isSuccess() ) sc = soSvc->fillRepRefs( addr, objs );
    objs->remove( part );
    delete objs;
    part = CaloClusterLink.next();
  }
}

// ============================================================================
// the end
// ============================================================================
