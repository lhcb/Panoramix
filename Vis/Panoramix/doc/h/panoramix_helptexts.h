/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**

@page panoramix_helptexts Help texts

 This section gather the various help texts (dialogs, help pulldown menu, etc...) of Panoramix. These help texts could
be found in the Panoramix/&lt;version&gt;/scripts/Help directory.

@section panoramix_helptexts_Geometry Geometry
@verbinclude Geometry.help

@section panoramix_helptexts_GaudiPython GaudiPython
@verbinclude GaudiPython.help

@section panoramix_helptexts_Tutorial Tutorial
@verbinclude Tutorial.help

*/
