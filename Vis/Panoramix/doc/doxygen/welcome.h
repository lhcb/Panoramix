/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**
@mainpage

 @image html Panoramix.gif
 @image latex Panoramix.ps

<center>
 @b Panoramix @b is @b an @b interactive @b environment
 @b for @b the @b LHCb @b experiment.
</center>

 (Will the cooking of C++, Python, XML, OpenGL, OpenInventor, Gaudi,
 OnX, etc, etc, etc... provide the magic potion ?)

@ref panoramix_introduction "Introduction"

@ref panoramix_packages "Packages, software organization"

@ref panoramix_download "Download and run (Linux and Windows)"

@ref panoramix_installation "Installation from source"

@ref panoramix_starting "Getting started"

@ref panoramix_gui "Graphical user interface"

@ref panoramix_navigation "Navigation, camera setting, picking"

@ref panoramix_helptexts "Help texts"

@ref panoramix_support "Support"

@ref panoramix_doc "Download manual, building the doc"

@ref panoramix_releasenotes "Release notes"

*/
