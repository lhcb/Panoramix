###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import (VisualizationSvc, OnXSvc)

## Detector and Event visualization :
ApplicationMgr().ExtSvc += ["SoDetSvc", "VisualizationSvc", "SoEventSvc"]

## Visualization attributes
VisualizationSvc().ColorDbLocation = "$XMLVISROOT/xml/colors.xml"

## Rich visualization :
# To be fixed in the upgrade, not implemented for the moment
#importOptions("$SORICHRECROOT/options/SoRichRec.opts")
# waiting to be fixed by Chris Jones, Jan.2009
#importOptions("$SORICHRECROOT/options/SoRichRecMC.opts")

## Calo visualization :
ApplicationMgr().ExtSvc += ["SoCaloSvc"]

## Histogram stat :
ApplicationMgr().ExtSvc += ["SoStatSvc"]

## The "interactivity" machinery ; OnXSvc (bridge Gaudi / OnX) :
importOptions("$ONXSVCROOT/options/OnXSvc.opts")
# OnXSvc.OutputToTerminal = false;

## The GUI XML file :
OnXSvc().File = "$PANORAMIXROOT/scripts/OnX/Panoramix.onx"
OnXSvc().Toolkit = "Qt"  #"NONE"
