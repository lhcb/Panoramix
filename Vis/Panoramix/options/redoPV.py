###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
#importOptions("$PATPVROOT/options/PVLoose.py")
from PatPV import PVConf
PVConf.LoosePV().configureAlg()

from Configurables import PatPVOffline
from Configurables import TESCheck, EventNodeKiller
PVCheck = TESCheck('PVCheck')
PVCheck.Inputs = ['Rec/Vertex']
eventNodeKiller = EventNodeKiller('PVkiller')
eventNodeKiller.Nodes = ['pRec/Vertex', 'Rec/Vertex']
removePV = GaudiSequencer("RemovePV", Members=[PVCheck, eventNodeKiller])
redoPV = GaudiSequencer(
    "RedoPV",
    IgnoreFilterPassed=True,
    Members=[removePV, PatPVOffline("PatPVOffline")])

DataOnDemandSvc().AlgMap["Rec/Vertex/Primary"] = redoPV
