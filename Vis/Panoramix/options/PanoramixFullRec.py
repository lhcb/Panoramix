###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import (ProcessPhase, GaudiSequencer, RecSysConf)
appConf = ApplicationMgr()

recsys = RecSysConf()
recsys.OutputLevel = 4

# Set main sequence
brunelSeq = GaudiSequencer("BrunelSequencer")
brunelSeq.MeasureTime = True
appConf.TopAlg += [brunelSeq]
brunelSeq.Members += [
    GaudiSequencer("CleanUpSequence"), "ProcessPhase/Init", "ProcessPhase/Reco"
]
ProcessPhase("Init").MeasureTime = True
ProcessPhase("Reco").MeasureTime = True
ProcessPhase("Init").DetectorList += ["Brunel", "Calo"]
# Convert Calo 'packed' banks to 'short' banks if needed
GaudiSequencer("InitCaloSeq").Members += ["GaudiSequencer/CaloBanksHandler"]
importOptions("$CALODAQROOT/options/CaloBankHandler.opts")

# run MC checking algorithms, needed for association tables
# however, only makes sense if FullRec was running
# need to find a way to get linker tables from DST
# importOptions("$PANORAMIXROOT/options/PanoramixMCchecks.opts")

from Configurables import TrackToDST
trackfilter = TrackToDST()
outputDSTSeq = GaudiSequencer("OutputDSTSeq")
outputDSTSeq.Members += [trackfilter]
appConf.TopAlg += [outputDSTSeq]
