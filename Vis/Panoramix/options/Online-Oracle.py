###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import appendPostConfigAction
from Configurables import ApplicationMgr, CondDB, COOLConfSvc

CORAL_XML_DIR = "/group/online/condb_viewer"
ApplicationMgr().Environment["CORAL_AUTH_PATH"] = CORAL_XML_DIR
ApplicationMgr().Environment["CORAL_DBLOOKUP_PATH"] = CORAL_XML_DIR

CondDB(UseOracle=True)
CondDB().UseLatestTags = ["2011"]


def disableLFC():
    COOLConfSvc(UseLFCReplicaSvc=False)
    CondDB().IgnoreHeartBeat = True


appendPostConfigAction(disableLFC)
