###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

segment_color = 'blue'


def Visualize_Rich_RecoSegments():

    #ui().echo('Visualising RICH Reconstructed Track Segments through the RICH Radiators')

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(segment_color)

    # Draw in all regions
    for region in range(Page().fPage.getNumberOfRegions()):

        # Move to each region in turn
        Page().setCurrentRegion(region)

        # Draw segments
        uiSvc().visualize('/Event/Rec/Rich/RecoEvent/Offline/Segments')

    # reset back
    Style().useVisSvc()
    Style().setColor(save_color)


def Visualize_Selected_Rich_RecoSegments(da):

    #ui().echo('Visualising associated RICH Reconstructed Track Segments through the RICH Radiators')

    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(segment_color)

    # draw the data
    data_visualize(da)

    # reset back
    Style().setColor(save_color)
