###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
fitterTool = getTool('TrackMasterFitter', 'ITrackFitter')
if not fitterTool:
    fitterTool = tsvc.create('TrackMasterFitter', interface='ITrackFitter')


def refit(tracklocation=''):
    if tracklocation == '':
        tracklocation = session().parameterValue('Track.location')
    if tracklocation == '': tracklocation = 'Rec/Track/Best'
    if evt[tracklocation]:
        for atrack in evt[tracklocation]:
            ##if atrack.fitStatus() == 1:
            ##  if atrack.type() != atrack.Velo:
            rc = fitterTool.fit(atrack)
