###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *


def Visualize_Rich_MCSegments():

    #ui().echo('Visualising all RICH MC Segments')
    #ui().echo(' -> Requires extended RICH data with addition MC information stored')

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')

    # Draw in all regions
    for region in range(Page().fPage.getNumberOfRegions()):

        # Move to each region in turn
        Page().setCurrentRegion(region)

        # draw data
        Style().setColor('green')
        uiSvc().visualize('/Event/MC/Rich/Segments')
        Style().setColor('darkgreen')
        uiSvc().visualize('/Event/Prev/MC/Rich/Segments')
        uiSvc().visualize('/Event/PrevPrev/MC/Rich/Segments')
        uiSvc().visualize('/Event/Next/MC/Rich/Segments')
        uiSvc().visualize('/Event/NextNext/MC/Rich/Segments')
        uiSvc().visualize('/Event/LHCBackground/MC/Rich/Segments')

    # reset back
    Style().useVisSvc()
    Style().setColor(save_color)


def Visualize_Selected_Rich_MCSegments(da):

    #ui().echo('Visualising selected RICH MC Segments')
    #ui().echo(' -> Requires extended RICH data with addition MC information stored')

    # Save current color
    save_color = session().parameterValue('modeling.color')

    Style().setColor('green')

    # draw segments
    data_visualize(da)

    # reset back
    Style().setColor(save_color)
