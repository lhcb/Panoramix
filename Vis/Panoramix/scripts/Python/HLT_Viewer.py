###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
from HLTPanoramix import *

item = ui().parameterValue('Panoramix_HLT_input_action.value')

if item == 'HLTHadron':
    ok = True
    d_lhcb('wh')
    s_modeling('s')
    d_hlt('L0TriggerHadron', 'red', 'ids')
    s_color('yellow')
    d_cluster('hcal')
    s_color('green')
    d_cluster('it')
    d_cluster('ot')
    d_hlt('HadSingleTRForward', 'blue', 'fscone')
if item == 'HLTMuon':
    ok = True
    d_lhcb('wh')
    s_modeling('s')
    d_hlt('L0TriggerMuon', 'red', 'ids')
    s_color('yellow')
    d_cluster('hcal')
    s_color('green')
    d_cluster('it')
    d_cluster('ot')
    d_hlt('HadSingleMuon', 'blue', 'fscone')
