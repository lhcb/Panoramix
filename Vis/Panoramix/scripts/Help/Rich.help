
Rich Editor Help
===================

This menu provides options to visualize various RICH data objects for the currently selected objects.

RichRecoCKRings
--------------------------
Visualise the associated Cherekov rings as projected onto the HPD detector panels. 
The rings are shown with the parts that are within the acceptance of an HPD (and thus
observable) coloured blue. The remaning parts, outside the acceptable of the HPDs are 
shown grey.

For each object (e.g. Track or Particle) more than one ring is drawn. Firstly rings are shown
for each radiator medium that the associated Track/Particle traversed and for which observable
CK photons where possible (Note this does not mean photons where observed, only that it was 
geometrically possible for them to be observed, if they had been).

Also, for each radiator one ring is drawn for each of the five possible mass hypotheses that were
above threshold.

RichRecoPixels
--------------------------
Visualise the RICH hits (magnified back from the HPD silicon chip to the point on the entry window where
the photon first entered the HPD) that are associated to the selected data objects. Association for 
Tracks or Particles means that valid reconstructed photon candiates where produced between that Track and 
the RICH hit. For reconstructed photons, the associated hit cluster is drawn. For CK rings, if the data 
object explicitly has a set of associated hits then these are visualised (for instance, the rich presents 
the hit to a set of hits). If the CK ring does not have any hits explicitly associated, but instead has 
an associated Track, then the hits associated to that track are visualized.


RichRecoSegments
--------------------------
Visualize the associated track radiator segments (The possible trajectories through each of the RICH 
radiator media) for the selected objects.

RichRecoPhotons
--------------------------
Visualize the associated reconstructed photon candidates for the selected data objects.

RichMCCKRings
--------------------------
Visualize the true MC Cherenkov rings for the selected data objects.
NB : Requires simulated data with the extended RICH information included.

RichMCPixels
--------------------------
Visualize the rich hits that are due to real Cherenkov signal radiation from the primary RICH
radiator media.

RichMCSegments
--------------------------
Visualize the true MC trajectories through the RICH radiator media, for the selected data objects.
NB : Requires simulated data with the extended RICH information included.

RichMCPhotons
--------------------------
Visualize the true Cherenkov photons for the selected data objects.
NB : Requires simulated data with the extended RICH information included.
