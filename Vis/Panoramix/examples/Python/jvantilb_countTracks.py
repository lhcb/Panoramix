###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Panoramix import *

#eraseEvent()
#uiSvc.nextEvent()

filter = '(unique==true)'
data_collect('TrStoredTrack', filter)

#  ss = std_vector_string()
#  sret = typeManager.execute('number',ss).c_str()
#  del ss
#  return sret

data_dump()
