###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *


def execute():
    ui().echo('Visualize Clusters ')

    session().setParameter('modeling.what', 'no')
    Style().setColor('white')
    data_collect(da(), 'VeloCluster', 'isR==true')
    data_visualize(da())
    Style().setColor('magenta')
    data_collect(da(), 'VeloCluster', 'isR==false')
    data_visualize(da())
    Style().dontUseVisSvc()
    Style().setColor('orange')
    uiSvc().visualize('/Event/Raw/TT/Clusters')
    Style().setColor('red')
    uiSvc().visualize('/Event/Raw/IT/Clusters')
    Style().setColor('green')
    uiSvc().visualize('/Event/Raw/OT/Times')
    Style().useVisSvc()

    ui().echo(' Velo R: white, Velo Phi purple')
    ui().echo(' TT: orange, IT: red, OT: green')
