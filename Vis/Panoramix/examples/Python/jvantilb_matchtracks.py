###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Panoramix import *

##################################################################
# Draw Velo, IT and OT for tracking event display                #
##################################################################

# Go to the next event
eraseEvent()
#uiSvc.nextEvent()

# Now draw the unique forward and match tracks
filter = 'match==true'
session.setParameter('modeling.what', 'no')
session.setParameter('modeling.useExtrapolator', 'yes')
session.setParameter('modeling.showCurve', 'yes')
session.setParameter('modeling.color', 'blue')
data_collect('TrStoredTrack', filter)
data_visualize()

# Draw their measurements
session.setParameter('modeling.what', 'ITMeasurements')
#session.setParameter('modeling.color','red')
data_collect('TrStoredTrack', filter)
data_visualize()
session.setParameter('modeling.what', 'no')

# Draw their measurements
session.setParameter('modeling.what', 'OTMeasurements')
#session.setParameter('modeling.color','red')
data_collect('TrStoredTrack', filter)
data_visualize()

session.setParameter('modeling.color', 'red')
session.setParameter('modeling.what', 'no')
