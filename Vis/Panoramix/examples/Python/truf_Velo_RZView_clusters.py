###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
TrackMasterFitter = appMgr.toolsvc().create(
    'TrackMasterFitter', interface='ITrackFitter')

if ui().findWidget('Viewer_2d'):
    ui().setCurrentWidget(ui().findWidget('Viewer_2d'))
    Page().setCurrentRegion(0)
    session().setParameter('modeling.projection', '-ZR')
    session().setParameter('modeling.what', 'no')

    Style().setColor('violet')
    data_collect(da(), 'VeloCluster', 'isR==false')
    data_visualize(da())

    Style().setColor('white')
    data_collect(da(), 'VeloCluster', 'isR==true')
    data_visualize(da())

    Style().setColor('yellow')
    session().setParameter('modeling.what', 'no')
    Style().setLineWidth(1.)

    Style().setColor('blue')
    session().setParameter('modeling.projection', '')
