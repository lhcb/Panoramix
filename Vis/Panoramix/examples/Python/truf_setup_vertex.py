###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

# Vertex view :
if not ui().findWidget('Viewer_vx'):
    ui().createComponent('Viewer_vx', 'PageViewer', 'ViewerTabStack')
    ui().setCallback('Viewer_vx', 'collect', 'DLD',
                     'OnX viewer_collect @this@')
    ui().setCallback('Viewer_vx', 'popup', 'DLD',
                     'Panoramix Panoramix_viewer_popup')
    ui().setParameter(
        'Viewer_vx.popupItems',
        'Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits'
    )

ui().setCurrentWidget(ui().findWidget('Viewer_vx'))
Page().setTitle('Vertex view')
Page().titleVisible(True)

# Setup region (a page can have multiple drawing region) :
Page().setCurrentRegion(0)
Viewer().setFrame()

Camera().setPosition(300., 0., 0.)
Camera().setHeight(100.)
Camera().setOrientation(0., -1., 0., 1.57)
Camera().setNearFar(-5000., 5000.)
Region().setTransformScale(1., 1., 1.)
Viewer().removeAutoClipping()
