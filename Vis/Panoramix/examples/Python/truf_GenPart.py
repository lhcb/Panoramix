###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *


def Exist(loc):
    # does not work as it should
    # anyhow, only a stupid hack
    result = True
    try:
        evt.leaves(loc)
    except:
        result = False
    return result


def execute():
    ui().echo('Visualize MCparticles ')
    ui().echo('gamma==white, rest=yellow')
    Style().setLineWidth(0)

    # gamma (white) :
    Style().setColor('white')
    data_collect(da(), 'MCParticle', 'particle=="gamma"')
    data_visualize(da())
    # charged (yellow) :
    Style().setColor('yellow')
    data_collect(da(), 'MCParticle', 'particle!="gamma"')
    data_visualize(da())

    Style().setColor('grey')

    if Exist('/Event/MC/OT/Hits'): uiSvc().visualize('/Event/MC/OT/Hits')
    if Exist('/Event/MC/IT/Hits'): uiSvc().visualize('/Event/MC/IT/Hits')
    if Exist('/Event/MC/TT/Hits'): uiSvc().visualize('/Event/MC/TT/Hits')
    if Exist('/Event/MC/Velo/Hits'): uiSvc().visualize('/Event/MC/Velo/Hits')
    if Exist('/Event/MC/PuVeto/Hits'):
        uiSvc().visualize('/Event/MC/PuVeto/Hits')
