###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
newcont = GaudiPython.makeClass(
    'KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> >'
)
velo_loc = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo'
velo = det[velo_loc]


def signRzTrack():
    if not evt['Rec/Track/RZVelo']: return
    signedrz_cont = newcont()
    GaudiPython.setOwnership(signedrz_cont, False)
    for t in evt['Rec/Track/RZVelo']:
        tnew = t.clone()
        l0 = tnew.lhcbIDs()[0]
        firstSensor = l0.veloID().sensor()
        if not velo.sensor(firstSensor).isLeft():
            s0 = tnew.states()[0]
            x = s0.x()
            tx = s0.tx()
            s0.setX(-x)
            s0.setTx(-tx)
        GaudiPython.setOwnership(tnew, False)
        signedrz_cont.add(tnew)
    evt.registerObject('Rec/Track/RZVeloSigned', signedrz_cont)
