###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

# Camera positioning :
Camera().setPosition(0., 0., 20000.)
Camera().setHeight(12000.)
Camera().setOrientation(0., 0., 1., 0.)
Camera().setNearFar(1., 100000.)

ui().echo(' Visualize Calo Digits')
Style().dontUseVisSvc()
Style().setColor('blue')
uiSvc().visualize('/Event/Raw/Hcal/Digits')
Style().setColor('red')
uiSvc().visualize('/Event/Raw/Ecal/Digits')
Style().setColor('green')
uiSvc().visualize('/Event/Raw/Prs/Digits')
Style().setColor('yellow')
uiSvc().visualize('/Event/Raw/Spd/Digits')
data_visualize(da())
Style().useVisSvc()
