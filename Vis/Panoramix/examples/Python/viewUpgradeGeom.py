###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, getopt


def usage():
    print(
        'Usage: python viewUpgradeGeom.py [-d <dddb_tag>] [-s <simcond_tag>]')


try:
    opts, args = getopt.getopt(sys.argv[1:], "d:s:h", [])
except getopt.GetoptError:
    usage()
    sys.exit()

DDDBtag = 'upgrade/master'
SIMCONDtag = 'upgrade/master'

for o, a in opts:
    if o == '-h':
        usage()
        sys.exit()
    elif o == '-d':
        DDDBtag = a
    elif o == '-s':
        SIMCONDtag = a

from Configurables import LHCbApp
from PanoramixSys.Configuration import *

importOptions('$PANORAMIXROOT/options/PanoramixVis.py')

lhcbApp = LHCbApp()

lhcbApp.DataType = 'Upgrade'
lhcbApp.Simulation = True
lhcbApp.DDDBtag = DDDBtag
lhcbApp.CondDBtag = SIMCONDtag

appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc', 'LHCb::ParticlePropertySvc']
# for particle property service
import PartProp.Service, PartProp.decorators

import GaudiPython as GP
import gaudigadgets

appMgr = GP.AppMgr()
appMgr.initialize()

from panoramixmodule import *
toui()
