###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *


def jtat_TrgRec_plot():
    session.setParameter('modeling.showCurve', 'true')
    session.setParameter('modeling.useExtrapolator', 'true')

    #session.setParameter('TrgTrack.location','Rec/Trg/VeloRTracks')
    #session.setParameter('modeling.lineWidth','2')
    #session_setColor('gold')
    #data_collect('TrgTrack')
    #data_visualize()

    session.setParameter('TrgTrack.location', 'Rec/Trg/DownstreamTracks')
    session.setParameter('modeling.lineWidth', '2')
    session_setColor('green')
    data_collect('TrgTrack')
    data_visualize()

    session.setParameter('TrgTrack.location', 'Rec/Trg/KShortTracks')
    session.setParameter('modeling.lineWidth', '2')
    session_setColor('blue')
    data_collect('TrgTrack')
    data_visualize()

    session.setParameter('TrgTrack.location', 'Rec/Trg/LongTracks')
    session.setParameter('modeling.lineWidth', '2')
    session_setColor('cyan')
    data_collect('TrgTrack')
    data_visualize()

    #session.setParameter('TrgTrack.location','Rec/Trg/VeloSpaceTracks')
    #session.setParameter('modeling.lineWidth','2')
    #session_setColor('cyan')
    #data_collect('TrgTrack')
    #data_visualize()

    session.setParameter('TrgTrack.location', 'Rec/Trg/VttTracks')
    session.setParameter('modeling.lineWidth', '2')
    session_setColor('orange')
    data_collect('TrgTrack')
    data_visualize()

    session.setParameter('modeling.lineWidth', '10')
    session_setColor('red')
    uiSvc.visualize('/Event/Rec/Trg/Vertex2D')

    session.setParameter('modeling.lineWidth', '2')
    session_setColor('white')
    data_collect('VeloCluster', 'isR==true')
    data_visualize()
    session_setColor('violet')
    data_collect('VeloCluster', 'isR==false')
    data_visualize()

    #session.setParameter('modeling.what','no')
    #session.setParameter('modeling.lineWidth','1')


page = ui.currentPage()
region = page.currentRegion()

jtat_TrgRec_plot()

del region
del page
