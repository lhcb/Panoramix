###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Panoramix import *

##################################################################
# Draw the OTClusters in the next event                          #
##################################################################

# Go to the next event
eraseEvent()
uiSvc.nextEvent()

# Draw the OTClusters
session.setParameter('modeling.color', 'red')
data_collect('OTCluster')
data_visualize()
