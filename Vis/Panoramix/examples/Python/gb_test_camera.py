###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Test script to position the camera.
#

import pmx

# gb_test_inventor :
#camera.setHeight(10)
#camera.setPosition(0,0,10)
#camera.setOrientation(0,1,0,0)

camera = pmx.Camera()
#camera.pointAt(2,2,0)
camera.lookAt(0, 0, -1)
