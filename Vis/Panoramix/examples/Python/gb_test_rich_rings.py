###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pmx

session = pmx.session()

style = pmx.Style()
da = pmx.DA()

style.setShape("center")
style.setColor("yellow")
da.collect('RichRecRing')
da.visualize()

style.setShape("inside")
style.setColor("blue")
da.collect('RichRecRing')
da.visualize()

style.setShape("outside")
style.setColor("red")
da.collect('RichRecRing')
da.visualize()
