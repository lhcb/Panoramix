###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pmx

da = pmx.DA()

stop_loop = 0


def next_event(i):
    pmx.ui().executeScript("DLD", "Panoramix next_event")
    #print 'debug : next_event %d' % i
    da.collect('MCParticle')
    da.visualize()
    pmx.ui().synchronize()


def start_loop():
    global stop_loop
    stop_loop = 0
    i = 0
    while stop_loop == 0:
        next_event(i)
        pmx.ui().synchronize()
        i = i + 1
