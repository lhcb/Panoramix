###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Panoramix import *

##################################################################
# Draw tracks and their measurements                             #
##################################################################

page = ui().currentPage()
region = page.currentRegion()
region.setParameter("dynamicScene", "clear")

# Now draw the unique forward and match tracks
filter = '((forward==true)||(match==true))&&(unique==true)'
#filter = '((upstream==true)||(seed==true))&&(unique==true)'
#filter = 'unique==true'
session().setParameter('modeling.what', 'no')
session().setParameter('modeling.useExtrapolator', 'yes')
session().setParameter('modeling.showCurve', 'yes')

Style().setColor('blue')
data_collect('Track', filter)
data_visualize(da())

## # Draw their measurements
session().setParameter('modeling.what', 'VeloMeasurements')
Style().setColor('cyan')
data_collect('Track', filter)
data_visualize(da())
session().setParameter('modeling.what', 'STMeasurements')
Style().setColor('red')
data_collect('Track', filter)
data_visualize(da())
session.setParameter('modeling.what', 'OTMeasurements')
Style().setColor('magenta')
data_collect('Track', filter)
data_visualize(da())

session.setParameter('modeling.what', 'no')
